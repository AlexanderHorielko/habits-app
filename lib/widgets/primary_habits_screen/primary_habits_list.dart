import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
// import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/primary_habits_bloc/primary_habits_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:intl/intl.dart';

class PrimaryHabitsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    PrimaryHabitsBloc primaryHabitsBloc = BlocProvider.of<PrimaryHabitsBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return FutureBuilder(
      future: primaryHabitsBloc.allPrimaryHabitsData(),
      builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return Container(
          height: 467 * settingsBloc.screenSizeRelation,
          padding: EdgeInsets.only(
            top: 30 * settingsBloc.screenSizeRelation,
            left: 15 * settingsBloc.screenSizeRelation,
            right: 15 * settingsBloc.screenSizeRelation,
          ),
          child: ListView.builder(
            itemCount: snapshot.data['primaryHabits'].length,
            itemBuilder: (context, index) {
              DocumentSnapshot currentGroup;

              snapshot.data['groups'].documents.forEach(
                (element) => {
                  if (element['name'] == snapshot.data['primaryHabits'][index].habitGroup)
                    {
                      currentGroup = element,
                    }
                },
              );

              return Container(
                decoration: BoxDecoration(
                  color: settingsBloc.theme == 'White theme' ? Colors.white : Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.circular(8),
                ),
                padding: EdgeInsets.only(
                  top: 20 * settingsBloc.screenSizeRelation,
                  bottom: 20 * settingsBloc.screenSizeRelation,
                ),
                margin: EdgeInsets.only(
                  bottom: 20 * settingsBloc.screenSizeRelation,
                ),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(snapshot.data['primaryHabits'][index].name),
                      leading: Container(
                        decoration: BoxDecoration(
                          color: HelperFunctions.buildColor(currentGroup.data['color']),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        width: 40 * settingsBloc.screenSizeRelation,
                        height: 40 * settingsBloc.screenSizeRelation,
                        child: Center(
                          child: Text(
                            '${currentGroup.data['name'][0].toUpperCase()}',
                            style: TextStyle(
                              color: HelperFunctions.buildColor(
                                        currentGroup.data['color'],
                                      ).computeLuminance() >
                                      0.5
                                  ? Colors.black
                                  : Colors.white,
                            ),
                          ),
                        ),
                      ),
                      trailing: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: Text('Start date'),
                          ),
                          Text(
                            DateFormat('dd.MM.yy').format(snapshot.data['primaryHabits'][index].created),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 16 * settingsBloc.screenSizeRelation,
                        right: 16 * settingsBloc.screenSizeRelation,
                        bottom: 12 * settingsBloc.screenSizeRelation,
                      ),
                      child: Divider(),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        left: 16 * settingsBloc.screenSizeRelation,
                        right: 16 * settingsBloc.screenSizeRelation,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text('Current streak'),
                                ),
                                Container(
                                  child: Text(
                                    primaryHabitsBloc
                                        .getCurrentStreak(
                                          snapshot.data['primaryHabits'][index],
                                          snapshot.data['days'],
                                        )
                                        .toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text('Best streak'),
                                ),
                                Container(
                                  child: Text(
                                    primaryHabitsBloc
                                        .getBestStreak(
                                          snapshot.data['primaryHabits'][index],
                                          snapshot.data['days'],
                                        )
                                        .toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text('Last break'),
                                ),
                                Container(
                                  child: Text(
                                    primaryHabitsBloc
                                        .getLastBreak(
                                          snapshot.data['primaryHabits'][index],
                                          snapshot.data['days'],
                                        )
                                        .toString(),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }
}
