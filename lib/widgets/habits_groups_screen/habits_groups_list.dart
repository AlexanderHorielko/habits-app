import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/habits_groups_bloc/habits_groups_bloc.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit_group.dart';
import 'package:habit/repositories/habit_repository.dart';
import 'package:habit/repositories/habits_group_repository.dart';
import 'package:habit/screens/add_group_screen.dart';

class HabitsGroupsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    HabitsGroupsBloc habitsGroupsBloc = BlocProvider.of<HabitsGroupsBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);

    return Builder(
      builder: (ctx) => StreamBuilder(
        stream: HabitsGroupRepository.getGroupsStream(authBloc.user),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshotGroups) {
          if (snapshotGroups.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return StreamBuilder(
            stream: HabitRepository.getHabitsStream(authBloc.user),
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshotHabits) {
              if (snapshotHabits.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              final allHabits = HabitRepository.transformToHabitsList(snapshotHabits.data);

              List<Map<String, dynamic>> habitsGroups = habitsGroupsBloc.getHabitsGroupsData(
                snapshotGroups,
                allHabits,
              );

              return Container(
                height: 370 * settingsBloc.screenSizeRelation,
                margin: EdgeInsets.only(
                  top: 30 * settingsBloc.screenSizeRelation,
                  left: 15 * settingsBloc.screenSizeRelation,
                ),
                child: ListView.builder(
                  itemCount: habitsGroups.length,
                  itemBuilder: (context, index) {
                    final habitWordEnding = HelperFunctions.getHabitsEnding(
                      habitsGroups[index]['habits'].length,
                    );
                    return Container(
                      height: 73 * settingsBloc.screenSizeRelation,
                      margin: EdgeInsets.only(
                        bottom: 15 * settingsBloc.screenSizeRelation,
                      ),
                      child: Dismissible(
                        key: ValueKey(index),
                        background: Container(
                          width: double.infinity,
                          color: Theme.of(context).errorColor,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              margin: EdgeInsets.only(
                                right: 20 * settingsBloc.screenSizeRelation,
                              ),
                              child: Icon(
                                Icons.delete,
                                size: 40 * settingsBloc.screenSizeRelation,
                                color: Theme.of(context).primaryColorLight,
                              ),
                            ),
                          ),
                        ),
                        direction: DismissDirection.endToStart,
                        onDismissed: (direction) async {
                          try {
                            final oldDays = await HabitsGroupRepository.deleteGroup(
                              habitsGroups[index]['id'],
                              habitsGroups[index]['habits'],
                            );
                            Scaffold.of(ctx).showSnackBar(
                              HelperFunctions.buildSnackBar(
                                ctx,
                                'Succefully deleted',
                                color: Theme.of(ctx).primaryColorDark,
                                deletedHabitGroup: HabitGroup(
                                  habitsGroups[index]['name'],
                                  habitsGroups[index]['color'],
                                  habits: habitsGroups[index]['habits'],
                                ),
                                oldDays: oldDays,
                              ),
                            );
                          } catch (e) {
                            Scaffold.of(ctx).showSnackBar(
                              HelperFunctions.buildSnackBar(
                                context,
                                e.message,
                                isError: true,
                              ),
                            );
                          }
                        },
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    height: 73 * settingsBloc.screenSizeRelation,
                                    decoration: BoxDecoration(
                                      color: settingsBloc.theme == 'White theme'
                                          ? Colors.white
                                          : Theme.of(context).primaryColorLight,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    padding: EdgeInsets.only(
                                      left: 15 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: 50 * settingsBloc.screenSizeRelation,
                                          height: 50 * settingsBloc.screenSizeRelation,
                                          margin: EdgeInsets.only(
                                            right: 30 * settingsBloc.screenSizeRelation,
                                          ),
                                          decoration: BoxDecoration(
                                            color: HelperFunctions.buildColor(
                                              habitsGroups[index]['color'],
                                            ),
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                          child: Center(
                                            child: Text(
                                              habitsGroups[index]['name'][0].toUpperCase(),
                                              style: TextStyle(
                                                color: HelperFunctions.buildColor(
                                                          habitsGroups[index]['color'],
                                                        ).computeLuminance() >
                                                        0.5
                                                    ? Colors.black
                                                    : Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              child: Text(
                                                '${habitsGroups[index]['name'][0].toUpperCase()}${habitsGroups[index]['name'].substring(1)}',
                                                style: TextStyle(
                                                  fontSize: 16 * settingsBloc.screenSizeRelation,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 5 * settingsBloc.screenSizeRelation,
                                            ),
                                            Container(
                                              child: Text(
                                                '${habitsGroups[index]['habits'].length} $habitWordEnding',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12 * settingsBloc.screenSizeRelation,
                                                  color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () {
                                    homeBloc.arguments = {
                                      'name': habitsGroups[index]['name'],
                                      'color': habitsGroups[index]['color'],
                                    };
                                    bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Habits'));
                                  },
                                ),
                              ),
                              GestureDetector(
                                child: Container(
                                  margin: EdgeInsets.only(
                                    left: 45 * settingsBloc.screenSizeRelation,
                                    right: 40 * settingsBloc.screenSizeRelation,
                                  ),
                                  child: SvgPicture.asset(
                                    'assets/images/gear.svg',
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                ),
                                onTap: () {
                                  homeBloc.arguments = {
                                    'name': habitsGroups[index]['name'],
                                    'color': habitsGroups[index]['color'],
                                  };
                                  // bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('AddGroup'));

                                  Navigator.of(context).pushNamed(
                                    AddGroupScreen.routeName,
                                    arguments: {
                                      'name': habitsGroups[index]['name'],
                                      'color': habitsGroups[index]['color'],
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
