import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/settings_screen/custom_switcher.dart';
import 'package:intl/intl.dart';

class RemindHabitsSwitch extends StatelessWidget {
  final AddHabitState state;

  RemindHabitsSwitch(this.state);
  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 20 * settingsBloc.screenSizeRelation,
            right: 106 * settingsBloc.screenSizeRelation,
          ),
          child: Row(
            children: <Widget>[
              Text(
                'Habits remind',
                style: TextStyle(
                  fontSize: 16 * settingsBloc.screenSizeRelation,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Spacer(),
              BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
                bloc: addHabitBloc,
                builder: (context, state) {
                  return CustomSwitch(
                    'HabitsRemind',
                    state,
                    addHabitBloc,
                  );
                },
              ),
            ],
          ),
        ),
        if (state.remindHabits)
          Container(
            padding: EdgeInsets.only(
              top: 35 * settingsBloc.screenSizeRelation,
            ),
            child: GestureDetector(
              child: Row(
                children: <Widget>[
                  Text(
                    'Select remind time',
                    style: TextStyle(
                      fontSize: 14 * settingsBloc.screenSizeRelation,
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).primaryColorDark.withOpacity(0.56),
                    ),
                  ),
                  Spacer(),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Theme.of(context).primaryColorDark,
                        ),
                      ),
                    ),
                    margin: EdgeInsets.only(
                      right: 102 * settingsBloc.screenSizeRelation,
                    ),
                    width: 50 * settingsBloc.screenSizeRelation,
                    child: Container(
                      margin: EdgeInsets.only(
                        bottom: 5 * settingsBloc.screenSizeRelation,
                      ),
                      child: Center(
                        child: Text(
                          DateFormat('Hm').format(
                            state.remindTime,
                          ),
                          style: TextStyle(
                            fontSize: 14 * settingsBloc.screenSizeRelation,
                            fontWeight: FontWeight.w400,
                            color: Theme.of(context).primaryColorDark,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              onTap: () {
                showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return Container(
                      height: MediaQuery.of(context).size.height / 3,
                      child: CupertinoDatePicker(
                        initialDateTime: state.remindTime,
                        use24hFormat: true,
                        mode: CupertinoDatePickerMode.time,
                        onDateTimeChanged: (time) {
                          addHabitBloc.emitEvent(
                            AddHabitEventTimePicked(
                              time,
                            ),
                          );
                        },
                      ),
                    );
                  },
                );
              },
            ),
          ),
      ],
    );
  }
}
