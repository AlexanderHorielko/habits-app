import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class NameInput extends StatelessWidget {
  final AddHabitState state;

  NameInput(this.state);
  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Container(
      margin: EdgeInsets.only(
        top: 60 * settingsBloc.screenSizeRelation,
      ),
      child: SizedBox(
        height: 30,
        child: AnimatedOpacity(
          opacity: addHabitBloc.opacity,
          duration: const Duration(milliseconds: 150),
          onEnd: () {
            addHabitBloc.opacity = 1;
            addHabitBloc.emitEvent(
              AddHabitEventInputChange(addHabitBloc.habit.name),
            );
            addHabitBloc.nameInputController.text = addHabitBloc.habit.name;
          },
          child: TextField(
            controller: addHabitBloc.nameInputController,
            style: TextStyle(
              fontSize: 20 * settingsBloc.screenSizeRelation,
              decoration: TextDecoration.none,
            ),
            decoration: InputDecoration(
              errorText:
                  state.name == null || state.name.length < 3 ? 'Habit name should have more than 2 characters' : null,
              hintText: 'Type habit`s name...',
              hintStyle: TextStyle(
                color: Theme.of(context).primaryColorDark,
              ),
              border: InputBorder.none,
            ),
            onChanged: (value) {
              addHabitBloc.emitEvent(
                AddHabitEventInputChange(value),
              );
            },
          ),
        ),
      ),
    );
  }
}
