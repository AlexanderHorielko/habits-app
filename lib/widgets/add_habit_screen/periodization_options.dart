// import 'package:flutter/material.dart';
// import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
// import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
// import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
// import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
// import 'package:habit/bloc/base/bloc_event_state_builder.dart';
// import 'package:habit/bloc/base/bloc_provider.dart';

// class PeriodiationOptions extends StatefulWidget {
//   PeriodiationOptions({Key key}) : super(key: key);

//   @override
//   _PeriodiationOptionsState createState() => _PeriodiationOptionsState();
// }

// class _PeriodiationOptionsState extends State<PeriodiationOptions>
//     with TickerProviderStateMixin {
//   Animation<double> showPeriodizationAnimation;
//   AnimationController showPeriodizationController;
//   @override
//   void initState() {
//     super.initState();
//     showPeriodizationController = AnimationController(
//         duration: const Duration(milliseconds: 150), vsync: this);
//     showPeriodizationAnimation = Tween<double>(begin: 0, end: 45).animate(
//       CurvedAnimation(
//         parent: showPeriodizationController,
//         curve: Curves.linear,
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
//     AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);

//     return BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
//       bloc: addHabitBloc,
//       builder: (context, state) {
//         return Container(
//           child: AnimatedBuilder(
//             animation: showPeriodizationAnimation,
//             builder: (BuildContext context, Widget child) {
//               state.showPeriodization
//                   ? showPeriodizationController.forward()
//                   : showPeriodizationController.reverse();
//               return Container(
//                 margin: EdgeInsets.only(
//                   top: 30 * settingsBloc.screenSizeRelation,
//                 ),
//                 child: Column(
//                   children: <Widget>[
//                     if (showPeriodizationAnimation.value > 0)
//                       GestureDetector(
//                         child: Container(
//                           height: showPeriodizationAnimation.value *
//                               authBloc.screenSizeRelation,
//                           child: Card(
//                             color: Theme.of(context).accentColor.withOpacity(
//                                 showPeriodizationAnimation.value / 45),
//                             child: Container(
//                               padding: EdgeInsets.all(10),
//                               width: double.infinity,
//                               child: Text(
//                                 'Every day',
//                                 style: TextStyle(
//                                   color: Theme.of(context).primaryColorLight,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () {
//                           addHabitBloc.emitEvent(
//                               AddHabitEventChoosePeriodization('Every day'));
//                         },
//                       ),
//                     if (showPeriodizationAnimation.value > 15)
//                       GestureDetector(
//                         child: Container(
//                           height: showPeriodizationAnimation.value *
//                               authBloc.screenSizeRelation,
//                           child: Card(
//                             color: Theme.of(context).accentColor.withOpacity(
//                                 showPeriodizationAnimation.value / 45),
//                             child: Container(
//                               padding: EdgeInsets.all(10),
//                               width: double.infinity,
//                               child: Text(
//                                 'In a day',
//                                 style: TextStyle(
//                                   color: Theme.of(context).primaryColorLight,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () {
//                           addHabitBloc.emitEvent(
//                               AddHabitEventChoosePeriodization('In a day'));
//                         },
//                       ),
//                     if (showPeriodizationAnimation.value > 30)
//                       GestureDetector(
//                         child: Container(
//                           height: showPeriodizationAnimation.value *
//                               authBloc.screenSizeRelation,
//                           child: Card(
//                             color: Theme.of(context).accentColor.withOpacity(
//                                 showPeriodizationAnimation.value / 45),
//                             child: Container(
//                               padding: EdgeInsets.all(10),
//                               width: double.infinity,
//                               child: Text(
//                                 'In 2 days',
//                                 style: TextStyle(
//                                   color: Theme.of(context).primaryColorLight,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () {
//                           addHabitBloc.emitEvent(
//                               AddHabitEventChoosePeriodization('In 2 days'));
//                         },
//                       ),
//                     if (showPeriodizationAnimation.value >= 45)
//                       GestureDetector(
//                         child: Container(
//                           height: showPeriodizationAnimation.value *
//                               authBloc.screenSizeRelation,
//                           child: Card(
//                             color: Theme.of(context).accentColor.withOpacity(
//                                 showPeriodizationAnimation.value / 45),
//                             child: Container(
//                               padding: EdgeInsets.all(10),
//                               width: double.infinity,
//                               child: Text(
//                                 'Once a week',
//                                 style: TextStyle(
//                                   color: Theme.of(context).primaryColorLight,
//                                 ),
//                               ),
//                             ),
//                           ),
//                         ),
//                         onTap: () {
//                           addHabitBloc.emitEvent(
//                               AddHabitEventChoosePeriodization('Once a week'));
//                         },
//                       ),
//                   ],
//                 ),
//               );
//             },
//           ),
//         );
//       },
//     );
//   }
// }
