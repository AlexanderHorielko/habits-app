import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class SendButton extends StatelessWidget {
  const SendButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
      bloc: addHabitBloc,
      builder: (context, state) {
        return Container(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(
                  16 * settingsBloc.screenSizeRelation,
                ),
                topRight: Radius.circular(
                  16 * settingsBloc.screenSizeRelation,
                ),
              ),
            ),
            padding: EdgeInsets.all(
              16 * settingsBloc.screenSizeRelation,
            ),
            color: Theme.of(context).accentColor,
            child: Center(
              child: Text(
                'Get the Habit',
                style: TextStyle(
                  color: Theme.of(context).primaryColorLight,
                  fontSize: 16 * settingsBloc.screenSizeRelation,
                ),
              ),
            ),
            onPressed: state.name == null || state.name.length < 3 || state.habitsGroup == ''
                ? null
                : () => addHabitBloc.sendHabit(context),
          ),
        );
      },
    );
  }
}
