import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class AllGroups extends StatelessWidget {
  final QuerySnapshot snapshot;
  final AddHabitState state;

  AllGroups(this.snapshot, this.state);

  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    final child = addHabitBloc.buildGroups(context, snapshot, state);
    return Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(
            bottom: 15 * settingsBloc.screenSizeRelation,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Select habits group',
                style: TextStyle(
                  fontSize: 16 * settingsBloc.screenSizeRelation,
                  fontWeight: FontWeight.w400,
                ),
              ),
              Container(
                child: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          ),
        ),
        Container(
          constraints: BoxConstraints(
            minHeight: 40 * settingsBloc.screenSizeRelation,
            maxHeight: 90 * settingsBloc.screenSizeRelation,
          ),
          width: double.infinity,
          child: ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 0),
            itemCount: 1,
            itemBuilder: (context, index) {
              return child;
            },
          ),
        ),
      ],
    );
  }
}
