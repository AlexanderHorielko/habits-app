import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/add_habit_screen/periodization_options_labels.dart';

class SelectPeriodization extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 65 * settingsBloc.screenSizeRelation,
          ),
          width: double.infinity,
          child: Text(
            'Periodization',
            style: TextStyle(
              fontSize: 16 * settingsBloc.screenSizeRelation,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        Container(
          // margin: EdgeInsets.only(
          //   top: 26 * settingsBloc.screenSizeRelation,
          // ),

          child: PeriodizationOptionsLabels(),
          // child: Column(
          //   children: <Widget>[
          //     Stack(
          //       children: <Widget>[
          //         GestureDetector(
          //           child: Container(
          //             decoration: BoxDecoration(
          //               border: Border(
          //                 bottom: BorderSide(
          //                   color: Theme.of(context).primaryColorDark,
          //                 ),
          //               ),
          //             ),
          //             child: Row(
          //               children: <Widget>[
          //                 Container(
          //                   margin: EdgeInsets.only(
          //                     bottom: 10 * settingsBloc.screenSizeRelation,
          //                   ),
          //                   child: Text(
          //                     'Select repetition frequency',
          //                     style: TextStyle(
          //                       fontSize: 14 * settingsBloc.screenSizeRelation,
          //                       color: Theme.of(context).primaryColorDark,
          //                     ),
          //                   ),
          //                 ),
          //                 Spacer(),
          //                 Container(
          //                   padding: EdgeInsets.only(
          //                     bottom: 6 * settingsBloc.screenSizeRelation,
          //                     right: 10 * settingsBloc.screenSizeRelation,
          //                   ),
          //                   child: Icon(
          //                     Icons.expand_more,
          //                     size: 20 * settingsBloc.screenSizeRelation,
          //                   ),
          //                 ),
          //               ],
          //             ),
          //           ),
          //           onTap: () {
          //             addHabitBloc.emitEvent(AddHabitEventShowPeriodization());
          //           },
          //         ),
          //         PeriodiationOptions(),
          //       ],
          //     ),
          //     PeriodizationOptionsLabels(),
          //   ],
          // ),
        ),
      ],
    );
  }
}
