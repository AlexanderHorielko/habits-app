import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class PeriodizationOptionsLabels extends StatefulWidget {
  PeriodizationOptionsLabels({Key key}) : super(key: key);

  @override
  _PeriodizationOptionsLabelsState createState() => _PeriodizationOptionsLabelsState();
}

class _PeriodizationOptionsLabelsState extends State<PeriodizationOptionsLabels> {
  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);

    return BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
      bloc: addHabitBloc,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.only(
            top: 15 * settingsBloc.screenSizeRelation,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                child: Text(
                  'Every day',
                  style: TextStyle(
                    fontSize: 14 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w400,
                    color: state.frequency == 'Every day'
                        ? Theme.of(context).primaryColorDark
                        : Theme.of(context).primaryColorDark.withOpacity(0.6),
                  ),
                ),
                onTap: () {
                  addHabitBloc.emitEvent(AddHabitEventChoosePeriodization('Every day'));
                },
              ),
              GestureDetector(
                child: Text(
                  'In a day',
                  style: TextStyle(
                    fontSize: 14 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w400,
                    color: state.frequency == 'In a day'
                        ? Theme.of(context).primaryColorDark
                        : Theme.of(context).primaryColorDark.withOpacity(0.6),
                  ),
                ),
                onTap: () {
                  addHabitBloc.emitEvent(AddHabitEventChoosePeriodization('In a day'));
                },
              ),
              GestureDetector(
                child: Text(
                  'In 2 days',
                  style: TextStyle(
                    fontSize: 14 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w400,
                    color: state.frequency == 'In 2 days'
                        ? Theme.of(context).primaryColorDark
                        : Theme.of(context).primaryColorDark.withOpacity(0.6),
                  ),
                ),
                onTap: () {
                  addHabitBloc.emitEvent(AddHabitEventChoosePeriodization('In 2 days'));
                },
              ),
              GestureDetector(
                child: Text(
                  'Once a week',
                  style: TextStyle(
                    fontSize: 14 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w400,
                    color: state.frequency == 'Once a week'
                        ? Theme.of(context).primaryColorDark
                        : Theme.of(context).primaryColorDark.withOpacity(0.6),
                  ),
                ),
                onTap: () {
                  addHabitBloc.emitEvent(AddHabitEventChoosePeriodization('Once a week'));
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
