import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/settings_screen/custom_switcher.dart';

class PrimarySwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Container(
      child: Row(
        children: <Widget>[
          Text(
            'Set as primary',
            style: TextStyle(
              fontSize: 16 * settingsBloc.screenSizeRelation,
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(
            width: 5 * settingsBloc.screenSizeRelation,
          ),
          Container(
            child: SvgPicture.asset(
              'assets/images/big_star_active.svg',
              height: 20 * settingsBloc.screenSizeRelation,
            ),
          ),
          Spacer(),
          Container(
            margin: EdgeInsets.only(
              right: 106 * settingsBloc.screenSizeRelation,
            ),
            child: BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
              bloc: addHabitBloc,
              builder: (context, state) {
                return CustomSwitch(
                  'Primary',
                  state,
                  addHabitBloc,
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
