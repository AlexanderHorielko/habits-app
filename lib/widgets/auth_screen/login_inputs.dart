import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/auth_bloc/auth_event.dart';
import 'package:habit/repositories/firebase_auth_repository.dart';

import '../../main.dart';

class LoginInputs extends StatelessWidget {
  final bool changeScreen;

  LoginInputs(this.changeScreen);

  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);

    MediaQueryData mediaQuery = MediaQuery.of(context);

    double screenSizeRelation = (mediaQuery.size.height / MyApp.defaultHeight);

    return Container(
      width: MediaQuery.of(context).size.width * 0.75,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              changeScreen ? 'Sign up' : 'Sign in',
              style: TextStyle(fontSize: 20 * screenSizeRelation, fontWeight: FontWeight.w500),
            ),
          ),
          StreamBuilder<Object>(
            stream: authBloc.email,
            builder: (context, snapshot) {
              return Container(
                margin: EdgeInsets.only(
                  bottom: snapshot.hasError ? 0 : 22 * screenSizeRelation,
                  top: 40 * screenSizeRelation,
                  right: 25 * screenSizeRelation,
                ),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(fontSize: 14 * screenSizeRelation),
                  decoration: InputDecoration(
                    errorText: snapshot.error,
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  onChanged: (value) {
                    authBloc.changeEmail(value);
                  },
                ),
              );
            },
          ),
          StreamBuilder<Object>(
            stream: authBloc.password,
            builder: (context, snapshot) {
              return Container(
                margin: EdgeInsets.only(
                  right: 25 * screenSizeRelation,
                  bottom: snapshot.hasError ? 8 * screenSizeRelation : 30 * screenSizeRelation,
                ),
                child: TextField(
                  obscureText: true,
                  style: TextStyle(fontSize: 14 * screenSizeRelation),
                  decoration: InputDecoration(
                    errorText: snapshot.error,
                    suffix: GestureDetector(
                      child: Text('forgot password?'),
                      onTap: () async {
                        await FirebaseAuthRepository.resetPassword(authBloc, context);
                      },
                    ),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  onChanged: (value) {
                    authBloc.changePassword(value);
                  },
                ),
              );
            },
          ),
          Container(
            margin: EdgeInsets.only(bottom: 22 * screenSizeRelation),
            child: Text(
              changeScreen ? 'Sign up with' : 'Sign in with',
              style: TextStyle(
                fontSize: 12 * screenSizeRelation,
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(40, 42, 53, 0.5),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 50 * screenSizeRelation),
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 40 * screenSizeRelation),
                  child: GestureDetector(
                    child: SvgPicture.asset('assets/images/google.svg'),
                    onTap: () {
                      authBloc.emitEvent(AuthEventSignUpWithGoogle());
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 40 * screenSizeRelation),
                  child: GestureDetector(
                    child: SvgPicture.asset('assets/images/facebook.svg'),
                    onTap: () {},
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 40 * screenSizeRelation),
                  child: GestureDetector(
                    child: SvgPicture.asset('assets/images/twitter.svg'),
                    onTap: () {},
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Text(
              changeScreen ? 'Have account?' : 'Don`t have account yet?',
              style: TextStyle(
                fontSize: 14 * screenSizeRelation,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
          ),
          GestureDetector(
            child: Container(
              child: Text(
                changeScreen ? 'Let`s sign in' : 'Let`s register you',
                style: TextStyle(
                  fontSize: 14 * screenSizeRelation,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
            onTap: () {
              authBloc.emitEvent(AuthEventChangeScreen());
            },
          ),
        ],
      ),
    );
  }
}
