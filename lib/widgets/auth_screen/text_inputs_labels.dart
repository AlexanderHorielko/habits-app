import 'package:flutter/material.dart';

import '../../main.dart';

class TextInputsLabels extends StatelessWidget {
  const TextInputsLabels({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);

    double screenSizeRelation = (mediaQuery.size.height / MyApp.defaultHeight);

    return Expanded(
      child: Container(
        margin: EdgeInsets.only(bottom: 100 * screenSizeRelation),
        padding: EdgeInsets.only(left: 15 * screenSizeRelation),
        width: MediaQuery.of(context).size.width * 0.25,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 55 * screenSizeRelation),
              child: Text(
                'Email',
                style: TextStyle(
                  color: Theme.of(context).primaryColorDark.withOpacity(0.7),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 25 * screenSizeRelation),
              child: Text(
                'Password',
                style: TextStyle(
                  color: Theme.of(context).primaryColorDark.withOpacity(0.7),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
