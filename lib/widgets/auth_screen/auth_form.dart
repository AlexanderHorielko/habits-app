import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/auth_bloc/auth_event.dart';
import 'package:habit/bloc/auth_bloc/auth_state.dart';
import 'package:habit/helpers/helper_functions.dart';

import '../../main.dart';
import 'text_inputs_labels.dart';
import 'login_inputs.dart';

class AuthForm extends StatefulWidget {
  final String error;

  AuthForm({this.error});

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  void initState() {
    super.initState();
    if (widget.error != null) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) async {
          Scaffold.of(context).showSnackBar(
            HelperFunctions.buildSnackBar(context, widget.error, isError: true),
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    AuthBloc bloc = BlocProvider.of<AuthBloc>(context);
    MediaQueryData mediaQuery = MediaQuery.of(context);
    double screenSizeRelation = (mediaQuery.size.height / MyApp.defaultHeight);

    return BlocEventStateBuilder(
      bloc: bloc,
      builder: (context, AuthState state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                top: mediaQuery.padding.top + 60 * screenSizeRelation,
                left: mediaQuery.size.width * 0.25,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Get',
                    style: TextStyle(fontSize: 40 * screenSizeRelation),
                  ),
                  Text(
                    'the habits',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 40 * screenSizeRelation,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                TextInputsLabels(),
                LoginInputs(state.changeScreen),
              ],
            ),
            StreamBuilder<Object>(
              stream: bloc.submit,
              builder: (context, snapshot) {
                return RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                    ),
                  ),
                  color: Theme.of(context).accentColor,
                  child: Container(
                    width: double.infinity,
                    height: 70 * screenSizeRelation,
                    child: Center(
                      child: Text(
                        state.changeScreen ? 'Sign up' : 'Sign in',
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 20 * screenSizeRelation,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                  onPressed: snapshot.hasData
                      ? () {
                          bloc.emitEvent(
                            state.changeScreen ? AuthEventSignUp() : AuthEventLogin(),
                          );
                        }
                      : null,
                );
              },
            ),
          ],
        );
      },
    );
  }
}
