import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_event.dart';

class CustomSwitch extends StatefulWidget {
  final String switcherName;
  final dynamic state;
  final dynamic bloc;

  CustomSwitch(this.switcherName, this.state, this.bloc);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;
  bool isReloaded = false;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: Duration(milliseconds: 0), vsync: this);
    animation = Tween<double>(begin: 0, end: 30).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.linear,
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return AnimatedBuilder(
      animation: animation,
      builder: (context, child) {
        if (widget.switcherName == 'Notification') {
          widget.state.notificationsOn ? controller.forward() : controller.reverse();
        } else if (widget.switcherName == 'InspirationText') {
          widget.state.inspirationTextOn ? controller.forward() : controller.reverse();
        } else if (widget.switcherName == 'Primary') {
          widget.state.isPrimary ? controller.forward() : controller.reverse();
        } else if (widget.switcherName == 'HabitsRemind') {
          widget.state.remindHabits ? controller.forward() : controller.reverse();
        }

        return GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                width: 60 * settingsBloc.screenSizeRelation,
                height: 30 * settingsBloc.screenSizeRelation,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorDark.withOpacity(0.03),
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
              Container(
                child: Container(
                  width: 30 * settingsBloc.screenSizeRelation,
                  height: 30 * settingsBloc.screenSizeRelation,
                  decoration: BoxDecoration(
                    color: animation.value > 15 ? Theme.of(context).accentColor : Theme.of(context).highlightColor,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Center(
                    child: Text(
                      animation.value > 15 ? 'ON' : 'OFF',
                      style: TextStyle(
                        color: animation.value > 15 ? Colors.white : Colors.black,
                        fontSize: 8 * settingsBloc.screenSizeRelation,
                      ),
                    ),
                  ),
                ),
                margin: EdgeInsets.only(left: animation.value),
              )
            ],
          ),
          onTap: () {
            switch (widget.switcherName) {
              case 'Notification':
                widget.bloc.emitEvent(SettingsEventSwitchNotifications());
                // settingsBloc.settingsWereChanged = true;
                break;
              case 'InspirationText':
                widget.bloc.emitEvent(SettingsEventSwitchInspirationText());
                // settingsBloc.settingsWereChanged = true;
                break;
              case 'Primary':
                if (addHabitBloc.isAddingPrimaryHabit) {
                  return;
                }
                widget.bloc.emitEvent(AddHabitEventSwitchPrimary());
                break;
              case 'HabitsRemind':
                widget.bloc.emitEvent(AddHabitEventSwitchHabitsRemind());
                break;
              default:
            }
          },
        );
      },
    );
  }
}
