import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/calendar_bloc/calendar_bloc.dart';
import 'package:habit/bloc/calendar_bloc/calendar_event.dart';
import 'package:habit/bloc/calendar_bloc/calendar_state.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/models/habit.dart';

class DailyHabits extends StatelessWidget {
  final List<Habit> habits;
  final QuerySnapshot days;

  DailyHabits(this.habits, this.days);
  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);

    return BlocEventStateBuilder<CalendarEvent, CalendarState>(
      bloc: calendarBloc,
      builder: (context, state) {
        int countDoneHabits = 0;
        DocumentSnapshot choosedDay;

        final choosedDayHabits = calendarBloc.getChoosedDayHabits(habits, days, state.choosedDate);
        choosedDay = calendarBloc.getChoosedDay(days, state);
        countDoneHabits = calendarBloc.countHabits(choosedDayHabits, choosedDay);

        return Container(
          margin: EdgeInsets.only(
            left: 16 * settingsBloc.screenSizeRelation,
            right: 25 * settingsBloc.screenSizeRelation,
          ),
          child: Column(
            children: <Widget>[
              Divider(),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 35 * settingsBloc.screenSizeRelation,
                    ),
                    child: Text(
                      'Completed habits',
                      style: TextStyle(
                        fontSize: 16 * settingsBloc.screenSizeRelation,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15 * settingsBloc.screenSizeRelation,
                  ),
                  Container(
                    child: Text(
                      choosedDayHabits == null ? '0 / 0' : '$countDoneHabits / ${choosedDayHabits.length}',
                      style: TextStyle(
                        fontSize: 16 * settingsBloc.screenSizeRelation,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              if (choosedDayHabits != null)
                Container(
                  height: 190 * settingsBloc.screenSizeRelation,
                  child: ListView.builder(
                    itemCount: choosedDayHabits.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(
                          bottom: 19 * settingsBloc.screenSizeRelation,
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              child: Text(
                                choosedDayHabits[index].name,
                                style: TextStyle(
                                  color: choosedDay.data['habits'][choosedDayHabits[index].id]
                                      ? Theme.of(context).primaryColorDark.withOpacity(0.6)
                                      : Theme.of(context).primaryColorDark,
                                  fontSize: 14 * settingsBloc.screenSizeRelation,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ),
                            choosedDayHabits[index].isPrimary
                                ? Container(
                                    margin: EdgeInsets.only(
                                      left: 10 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Icon(
                                      Icons.star_border,
                                      size: 15 * settingsBloc.screenSizeRelation,
                                    ),
                                  )
                                : Container(),
                            Spacer(),
                            Container(
                              child: SvgPicture.asset(
                                'assets/images/check.svg',
                                width: 12 * settingsBloc.screenSizeRelation,
                                color: choosedDay.data['habits'][choosedDayHabits[index].id]
                                    ? Theme.of(context).accentColor
                                    : Theme.of(context).primaryColorDark.withOpacity(0.6),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
