import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/calendar_bloc/calendar_bloc.dart';
import 'package:habit/bloc/calendar_bloc/calendar_event.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/widgets/calendar_screen/daily_habits.dart';
import 'package:habit/widgets/calendar_screen/streak_day.dart';
import 'package:table_calendar/table_calendar.dart';
import 'days.dart';

class ExpandedCalendar extends StatefulWidget {
  final CalendarController _calendarController;
  final QuerySnapshot allDays;
  final List<Habit> habits;

  ExpandedCalendar(this._calendarController, this.allDays, this.habits);

  @override
  _ExpandedCalendarState createState() => _ExpandedCalendarState();
}

class _ExpandedCalendarState extends State<ExpandedCalendar> {
  DateTime previousChoosedDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
  );
  @override
  Widget build(BuildContext context) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);
    // AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    calendarBloc.getAllStreakDays(widget.allDays);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: 16 * settingsBloc.screenSizeRelation,
            right: 16 * settingsBloc.screenSizeRelation,
          ),
          child: TableCalendar(
            availableCalendarFormats: {
              CalendarFormat.month: 'Month',
            },
            headerStyle: HeaderStyle(
              leftChevronIcon: Icon(
                Icons.chevron_left,
                color: Theme.of(context).primaryColorDark,
              ),
              rightChevronIcon: Icon(
                Icons.chevron_right,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
            events: calendarBloc.habitsMaps,
            holidays: calendarBloc.habitsMaps,
            rowHeight: 40 * settingsBloc.screenSizeRelation,
            calendarController: widget._calendarController,
            calendarStyle: CalendarStyle(
              contentPadding: EdgeInsets.symmetric(
                horizontal: 0 * settingsBloc.screenSizeRelation,
              ),
            ),
            onDaySelected: (day, events) {
              _onDaySelected(day);
            },
            onCalendarCreated: (first, last, format) {
              _onCalendarCreated(first, last, format);
            },
            startingDayOfWeek: StartingDayOfWeek.monday,
            builders: CalendarBuilders(
              todayDayBuilder: (context, date, events) {
                return StreakDay(settingsBloc, date, builder: 'today');
                // return calendarBloc.buildStreakDay(context, date, authBloc, builder: 'today');
              },
              dayBuilder: (context, date, events) {
                return Days(widget.allDays, date);
              },
              selectedDayBuilder: (context, date, events) {
                return Center(
                  child: Container(
                    width: 40 * settingsBloc.screenSizeRelation,
                    height: 40 * settingsBloc.screenSizeRelation,
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Center(
                      child: Text(
                        date.day.toString(),
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 16 * settingsBloc.screenSizeRelation,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                );
              },
              markersBuilder: (context, date, events, holidays) {
                return _markers(settingsBloc, date);
              },
              dowWeekdayBuilder: (context, weekday) {
                return _dowWeekDay(settingsBloc, weekday);
              },
            ),
          ),
        ),
        DailyHabits(widget.habits, widget.allDays),
      ],
    );
  }

  List<Widget> _markers(SettingsBloc settingsBloc, DateTime date) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);
    final dateKey = DateTime(date.year, date.month, date.day);

    DocumentSnapshot thisDay;
    bool isAllDone = true;
    final children = <Widget>[];

    widget.allDays.documents.forEach((element) {
      final fetchedDate = HelperFunctions.getDateTimeFromTimestamp(element.data['date']);

      if (fetchedDate == date) {
        thisDay = element;
      }
    });

    if (thisDay != null) {
      thisDay.data['habits'].forEach(
        (key, value) {
          if (isAllDone != false) {
            if (value == false) {
              isAllDone = false;
            }
          }
        },
      );
    }

    if (thisDay == null || thisDay.data['habits'].length == 0) {
      return List<Widget>();
    } else if (isAllDone) {
      children.add(
        Positioned(
          right: 12 * settingsBloc.screenSizeRelation,
          top: 6 * settingsBloc.screenSizeRelation,
          child: SvgPicture.asset(
            'assets/images/check.svg',
            width: 7 * settingsBloc.screenSizeRelation,
            color: calendarBloc.habitsMaps[dateKey].last['choosed']
                ? Theme.of(context).primaryColorLight
                : Theme.of(context).accentColor,
          ),
        ),
      );
    } else if (!isAllDone) {
      children.add(
        Positioned(
          right: 18 * settingsBloc.screenSizeRelation,
          top: 6 * settingsBloc.screenSizeRelation,
          child: Container(
            decoration: BoxDecoration(
              color: calendarBloc.habitsMaps[dateKey].last['choosed']
                  ? Theme.of(context).primaryColorLight
                  : Theme.of(context).accentColor,
              borderRadius: BorderRadius.circular(10),
            ),
            width: 4 * settingsBloc.screenSizeRelation,
            height: 4 * settingsBloc.screenSizeRelation,
          ),
        ),
      );
    }

    return children;
  }

  Widget _dowWeekDay(SettingsBloc settingsBloc, String weekday) {
    final day = weekday.toLowerCase();
    return Center(
      child: Column(
        children: <Widget>[
          Container(
            margin: weekday == 'Mon'
                ? EdgeInsets.only(right: 23 * settingsBloc.screenSizeRelation)
                : weekday == 'Sun'
                    ? EdgeInsets.only(left: 28 * settingsBloc.screenSizeRelation)
                    : weekday == 'Tue'
                        ? EdgeInsets.only(
                            right: 10 * settingsBloc.screenSizeRelation,
                          )
                        : weekday == 'Sat'
                            ? EdgeInsets.only(
                                left: 10 * settingsBloc.screenSizeRelation,
                              )
                            : EdgeInsets.only(right: 0),
            child: Text(
              day,
              style: TextStyle(fontSize: 14 * settingsBloc.screenSizeRelation),
            ),
          ),
          SizedBox(
            height: 5 * settingsBloc.screenSizeRelation,
          ),
          Divider()
        ],
      ),
    );
  }

  void _onCalendarCreated(DateTime first, DateTime last, CalendarFormat format) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);

    final dateKeyFirst = DateTime(first.year, first.month, first.day);
    final dateKeyLast = DateTime(last.year, last.month, last.day);
    final difference = dateKeyLast.difference(dateKeyFirst);

    DateTime firstDate = first;
    for (var i = 0; i <= difference.inDays; i++) {
      final dateKey = DateTime(
        firstDate.year,
        firstDate.month,
        firstDate.day,
      );
      firstDate = firstDate.add(const Duration(days: 1));
      if (calendarBloc.habitsMaps[dateKey] != null) {
        calendarBloc.habitsMaps[dateKey].add({'choosed': false, 'combo': false});
      } else {
        calendarBloc.habitsMaps[dateKey] = [
          {'choosed': false, 'combo': false}
        ];
      }
    }

    final today = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    );
    calendarBloc.habitsMaps[today].last['choosed'] = true;
  }

  void _onDaySelected(DateTime day) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);
    final dateKey = DateTime(day.year, day.month, day.day);
    if (calendarBloc.habitsMaps[previousChoosedDate] == null) {
      calendarBloc.habitsMaps[previousChoosedDate] = [];
      calendarBloc.habitsMaps[previousChoosedDate].add({});
    }
    if (calendarBloc.habitsMaps[dateKey] == null) {
      calendarBloc.habitsMaps[dateKey] = [];
      calendarBloc.habitsMaps[dateKey].add({});
    }
    calendarBloc.habitsMaps[previousChoosedDate].last['choosed'] = false;
    previousChoosedDate = dateKey;
    calendarBloc.habitsMaps[dateKey].last['choosed'] = true;
    calendarBloc.choosedDate = dateKey;
    calendarBloc.emitEvent(CalendarEventShowHabits());
  }
}
