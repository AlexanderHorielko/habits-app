import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/calendar_bloc/calendar_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

import 'day_number.dart';

class StreakDay extends StatefulWidget {
  final DateTime date;
  final SettingsBloc settingsBloc;
  final String builder;

  StreakDay(this.settingsBloc, this.date, {this.builder});

  @override
  _StreakDayState createState() => _StreakDayState();
}

class _StreakDayState extends State<StreakDay> {
  @override
  Widget build(BuildContext context) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);

    Widget child;
    final buildingDay = DateTime(
      widget.date.year,
      widget.date.month,
      widget.date.day,
    );

    final containsDay = calendarBloc.streakDates.contains(buildingDay);
    final containsPreviousDay = calendarBloc.streakDates.contains(buildingDay.subtract(const Duration(days: 1)));
    final containsNextDay = calendarBloc.streakDates.contains(buildingDay.add(const Duration(days: 1)));

    if (containsDay) {
      if (containsNextDay) {
        child = buildStreakDay(context, widget.settingsBloc, widget.date, 'next');
      }
      if (containsPreviousDay) {
        child = buildStreakDay(context, widget.settingsBloc, widget.date, 'previous');
      }
      if (containsPreviousDay && containsNextDay) {
        child = buildStreakDay(context, widget.settingsBloc, widget.date, 'middle');
      }
      if (!containsPreviousDay && !containsNextDay) {
        child = buildCommonDay(context, widget.settingsBloc, widget.date, widget.builder);
      }
    } else {
      child = buildCommonDay(context, widget.settingsBloc, widget.date, widget.builder);
    }
    return child;
  }
}

Widget buildStreakDay(BuildContext context, SettingsBloc settingsBloc, DateTime date, String streakDay) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 1),
    width: 58 * settingsBloc.screenSizeRelation,
    height: 40 * settingsBloc.screenSizeRelation,
    decoration: BoxDecoration(
      color: Theme.of(context).highlightColor,
      borderRadius: streakDay == 'middle'
          ? BorderRadius.zero
          : BorderRadius.only(
              topLeft: streakDay == 'next' ? Radius.circular(10) : Radius.circular(0),
              bottomLeft: streakDay == 'next' ? Radius.circular(10) : Radius.circular(0),
              topRight: streakDay == 'previous' ? Radius.circular(10) : Radius.circular(0),
              bottomRight: streakDay == 'previous' ? Radius.circular(10) : Radius.circular(0),
            ),
    ),
    child: DayNumber(settingsBloc, date),
  );
}

Widget buildCommonDay(BuildContext context, SettingsBloc settingsBloc, DateTime date, String builder) {
  Widget child;
  if (builder == 'today') {
    child = Center(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: Theme.of(context).accentColor,
          ),
        ),
        width: 40 * settingsBloc.screenSizeRelation,
        height: 40 * settingsBloc.screenSizeRelation,
        child: DayNumber(settingsBloc, date),
      ),
    );
  } else {
    child = Center(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 1),
        width: 40 * settingsBloc.screenSizeRelation,
        height: 40 * settingsBloc.screenSizeRelation,
        child: Center(
          child: date.month != DateTime.now().month
              ? Text(
                  date.day.toString(),
                  style: TextStyle(
                    color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                    fontSize: 16 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w500,
                  ),
                )
              : Text(
                  date.day.toString(),
                  style: TextStyle(
                    fontSize: 16 * settingsBloc.screenSizeRelation,
                    fontWeight: FontWeight.w500,
                  ),
                ),
        ),
      ),
    );
  }
  return child;
}
