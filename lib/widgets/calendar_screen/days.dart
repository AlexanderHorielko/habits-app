import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/calendar_bloc/calendar_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/calendar_screen/streak_day.dart';

class Days extends StatelessWidget {
  final QuerySnapshot days;
  final DateTime date;

  Days(this.days, this.date);

  @override
  Widget build(BuildContext context) {
    CalendarBloc calendarBloc = BlocProvider.of<CalendarBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    Widget child;

    calendarBloc.getAllStreakDays(days);
    child = StreakDay(settingsBloc, date, builder: 'day');
    // child =
    // calendarBloc.buildStreakDay(context, date, authBloc, builder: 'day');

    return child;
  }
}
