import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:intl/intl.dart';

class CalendarNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 16 * settingsBloc.screenSizeRelation,
      ),
      child: Container(
        margin: EdgeInsets.only(
          bottom: 40 * settingsBloc.screenSizeRelation,
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                right: 37 * settingsBloc.screenSizeRelation,
              ),
              child: Text(
                DateFormat.MMMM().format(DateTime.now()),
                style: TextStyle(
                  fontSize: 20 * settingsBloc.screenSizeRelation,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            GestureDetector(
              child: Container(
                width: 30,
                child: Icon(Icons.chevron_left),
              ),
            ),
            GestureDetector(
              child: Container(
                width: 30,
                child: Icon(Icons.chevron_right),
              ),
            ),
            Spacer(),
            Container(
              child: Text(
                DateTime.now().year.toString(),
                style: TextStyle(
                  fontSize: 20 * settingsBloc.screenSizeRelation,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
