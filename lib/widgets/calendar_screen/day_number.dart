import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class DayNumber extends StatelessWidget {
  final SettingsBloc settingsBloc;
  final DateTime date;

  DayNumber(this.settingsBloc, this.date);

  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    final difference = date.difference(DateTime.now()).inDays.abs();
    return Center(
      child: Text(
        date.day.toString(),
        style: TextStyle(
          color: difference != 0
              ? settingsBloc.theme == 'White theme'
                  ? Theme.of(context).primaryColorDark
                  : Theme.of(context).primaryColorLight
              : settingsBloc.theme == 'White theme' ? Colors.black : Colors.white,
          fontSize: 16 * settingsBloc.screenSizeRelation,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
