import 'package:flutter/material.dart';
import 'package:habit/bloc/add_group_bloc/add_group_bloc.dart';
import 'package:habit/bloc/add_group_bloc/add_group_event.dart';
import 'package:habit/bloc/add_group_bloc/add_group_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class GroupNameInput extends StatefulWidget {
  final String name;

  GroupNameInput({this.name});

  @override
  _GroupNameInputState createState() => _GroupNameInputState();
}

class _GroupNameInputState extends State<GroupNameInput> {
  FocusNode myFocusNode = FocusNode();
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    AddGroupBloc _addGroupBloc = BlocProvider.of<AddGroupBloc>(context);
    if (widget.name != null) {
      _addGroupBloc.emitEvent(
        AddGroupEventChangeName(widget.name),
      );
      _controller.text = widget.name;
    }
    return BlocEventStateBuilder<AddGroupEvent, AddGroupState>(
      bloc: _addGroupBloc,
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.only(
            top: 25 * settingsBloc.screenSizeRelation,
            bottom: 50 * settingsBloc.screenSizeRelation,
          ),
          child: TextField(
            controller: _controller,
            focusNode: myFocusNode,
            decoration: InputDecoration(
              errorText: state.name.length < 3 ? 'Habit group name should have more than 2 characters' : null,
              labelText: 'Enter groupname: ',
              labelStyle: TextStyle(
                color: myFocusNode.hasFocus
                    ? Theme.of(context).accentColor
                    : Theme.of(context).primaryColorDark.withOpacity(0.6),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Theme.of(context).accentColor,
                ),
              ),
            ),
            onChanged: (value) {
              _addGroupBloc.emitEvent(
                AddGroupEventChangeName(value),
              );
            },
          ),
        );
      },
    );
  }
}
