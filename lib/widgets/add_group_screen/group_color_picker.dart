import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:habit/bloc/add_group_bloc/add_group_bloc.dart';
import 'package:habit/bloc/add_group_bloc/add_group_event.dart';
import 'package:habit/bloc/add_group_bloc/add_group_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';

class GroupColorPicker extends StatefulWidget {
  final List<dynamic> colorValues;

  GroupColorPicker({this.colorValues});
  @override
  _GroupColorPickerState createState() => _GroupColorPickerState();
}

class _GroupColorPickerState extends State<GroupColorPicker> {
  List<dynamic> color = [];

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    AddGroupBloc _addGroupBloc = BlocProvider.of<AddGroupBloc>(context);
    if (widget.colorValues != null) {
      _addGroupBloc.emitEvent(
        AddGroupEventChangeColor(
          widget.colorValues,
        ),
      );
    }
    return BlocEventStateBuilder<AddGroupEvent, AddGroupState>(
      bloc: _addGroupBloc,
      builder: (context, state) {
        return Row(
          children: <Widget>[
            RaisedButton(
              color: Theme.of(context).highlightColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Text(
                'Choose group color',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              onPressed: () {
                FocusScope.of(context).unfocus();
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      content: Container(
                        height: MediaQuery.of(context).size.height / 1.65,
                        child: ColorPicker(
                          pickerColor: state.color != null ? HelperFunctions.buildColor(state.color) : Colors.black,
                          onColorChanged: (value) {
                            color = [];
                            color.add(value.red);
                            color.add(value.green);
                            color.add(value.blue);
                            color.add(value.opacity);
                          },
                        ),
                      ),
                      actions: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(
                            bottom: 7 * settingsBloc.screenSizeRelation,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Container(
                                child: RaisedButton(
                                  color: Theme.of(context).highlightColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Text(
                                    'Close',
                                    style: TextStyle(
                                      color: Colors.black,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                              Container(
                                height: 36 * settingsBloc.screenSizeRelation,
                                width: 52 * settingsBloc.screenSizeRelation,
                              ),
                              Container(
                                child: RaisedButton(
                                  color: Theme.of(context).accentColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Text(
                                    'Choose',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorLight,
                                    ),
                                  ),
                                  onPressed: () {
                                    _addGroupBloc.emitEvent(
                                      AddGroupEventChangeColor(
                                        color,
                                      ),
                                    );
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    );
                  },
                );
              },
            ),
            Spacer(),
            Container(
              width: 38 * settingsBloc.screenSizeRelation,
              height: 38 * settingsBloc.screenSizeRelation,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 5,
                  ),
                ],
                color: state.color == null
                    ? Theme.of(context).accentColor
                    : Color.fromRGBO(
                        state.color[0],
                        state.color[1],
                        state.color[2],
                        double.parse(state.color[3].toString()),
                      ),
                borderRadius: BorderRadius.circular(200),
              ),
            ),
          ],
        );
      },
    );
  }
}
