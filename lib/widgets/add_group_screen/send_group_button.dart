import 'package:flutter/material.dart';
import 'package:habit/bloc/add_group_bloc/add_group_bloc.dart';
import 'package:habit/bloc/add_group_bloc/add_group_event.dart';
import 'package:habit/bloc/add_group_bloc/add_group_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';

class SendGroupButton extends StatelessWidget {
  final String title;
  final String oldName;

  SendGroupButton({this.title, this.oldName});

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    AddGroupBloc addGroupBloc = BlocProvider.of<AddGroupBloc>(context);

    return BlocEventStateBuilder<AddGroupEvent, AddGroupState>(
      bloc: addGroupBloc,
      builder: (context, state) {
        if (state.error != null) {
          Scaffold.of(context).showSnackBar(
            HelperFunctions.buildSnackBar(context, state.error),
          );
        }
        return Container(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(
                  16 * settingsBloc.screenSizeRelation,
                ),
                topRight: Radius.circular(
                  16 * settingsBloc.screenSizeRelation,
                ),
              ),
            ),
            padding: EdgeInsets.all(
              16 * settingsBloc.screenSizeRelation,
            ),
            color: Theme.of(context).accentColor,
            child: Center(
              child: Text(
                title != null ? title : 'Add group',
                style: TextStyle(
                  color: Theme.of(context).primaryColorLight,
                  fontSize: 16 * settingsBloc.screenSizeRelation,
                ),
              ),
            ),
            onPressed: state.name.length < 3 ? null : () => addGroupBloc.sendGroup(title, oldName, context),
          ),
        );
      },
    );
  }
}
