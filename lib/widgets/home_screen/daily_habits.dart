import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/home_bloc/home_event.dart';
import 'package:habit/bloc/home_bloc/home_state.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/models/habit.dart';
import 'package:intl/intl.dart';

class DailyHabits extends StatefulWidget {
  const DailyHabits({Key key}) : super(key: key);

  @override
  _DailyHabitsState createState() => _DailyHabitsState();
}

class _DailyHabitsState extends State<DailyHabits> {
  @override
  Widget build(BuildContext context) {
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return FutureBuilder(
      future: homeBloc.getTodayHabits(context),
      builder: (context, AsyncSnapshot<List<Habit>> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        snapshot.data.forEach((element) {
          homeBloc.dailyHabitsIsDone[element.id] = element.isDone;
        });

        if (snapshot.data.length == 0) {
          return Container(
            height: 315 * settingsBloc.screenSizeRelation,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 15 * settingsBloc.screenSizeRelation),
                  child: SvgPicture.asset(
                    'assets/images/no_habits.svg',
                    height: 180 * settingsBloc.screenSizeRelation,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20 * settingsBloc.screenSizeRelation),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Seems like your ',
                          style: TextStyle(
                            fontSize: 16 * settingsBloc.screenSizeRelation,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          'habit list empty',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16 * settingsBloc.screenSizeRelation,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 5 * settingsBloc.screenSizeRelation),
                  child: Text(
                    'Let`s get your first habit',
                    style: TextStyle(
                      color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                      fontSize: 14 * settingsBloc.screenSizeRelation,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 10 * settingsBloc.screenSizeRelation,
                    left: 176 * settingsBloc.screenSizeRelation,
                  ),
                  child: SvgPicture.asset(
                    'assets/images/rounded_arrow.svg',
                  ),
                ),
              ],
            ),
          );
        }
        return Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 16 * settingsBloc.screenSizeRelation,
                  vertical: 25 * settingsBloc.screenSizeRelation,
                ),
                child: Row(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          'Daily ',
                          style: TextStyle(
                            fontSize: 16 * settingsBloc.screenSizeRelation,
                          ),
                        ),
                        Text(
                          'habits',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16 * settingsBloc.screenSizeRelation,
                          ),
                        )
                      ],
                    ),
                    Spacer(),
                    BlocEventStateBuilder(
                      bloc: homeBloc,
                      builder: (context, HomeState state) {
                        homeBloc.emitEvent(HomeEventTimerChange());
                        if (state.loading != null) {
                          return Text(
                            DateFormat.Hms().format(DateTime.now()),
                            style: TextStyle(
                              color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                              fontSize: 14 * settingsBloc.screenSizeRelation,
                            ),
                          );
                        } else if (state.time != null) {
                          return Container(
                            child: Text(
                              DateFormat.Hms().format(DateTime.now()),
                              style: TextStyle(
                                color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                fontSize: 14 * settingsBloc.screenSizeRelation,
                              ),
                            ),
                          );
                        } else if (state.error != null) {
                          return Container();
                        }
                        return Container();
                      },
                    ),
                  ],
                ),
              ),
              Container(
                height: 200 * settingsBloc.screenSizeRelation,
                child: ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.only(
                          right: 0,
                          left: 15 * settingsBloc.screenSizeRelation,
                          bottom: 0,
                        ),
                        leading: FutureBuilder(
                          future: homeBloc.getGroup(snapshot.data[index].habitGroup),
                          builder: (context, snap) {
                            if (snap.connectionState == ConnectionState.waiting) {
                              return Container(
                                width: 40 * settingsBloc.screenSizeRelation,
                                height: 40 * settingsBloc.screenSizeRelation,
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              );
                            }

                            Color color = Color.fromRGBO(
                              snap.data['color'][0],
                              snap.data['color'][1],
                              snap.data['color'][2],
                              homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                  ? double.parse(
                                        snap.data['color'][3].toString(),
                                      ) *
                                      0.6
                                  : double.parse(
                                      snap.data['color'][3].toString(),
                                    ),
                            );
                            return BlocEventStateBuilder<HomeEvent, HomeState>(
                              bloc: homeBloc,
                              builder: (context, state) {
                                return Container(
                                  width: 40 * settingsBloc.screenSizeRelation,
                                  height: 40 * settingsBloc.screenSizeRelation,
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(
                                      snap.data['color'][0],
                                      snap.data['color'][1],
                                      snap.data['color'][2],
                                      homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                          ? double.parse(
                                                snap.data['color'][3].toString(),
                                              ) *
                                              0.6
                                          : double.parse(
                                              snap.data['color'][3].toString(),
                                            ),
                                    ),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Center(
                                    child: Text(
                                      snap.data['name'][0].toUpperCase(),
                                      style: TextStyle(
                                        color: color.computeLuminance() > 0.5 ? Colors.black : Colors.white,
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        ),
                        title: BlocEventStateBuilder<HomeEvent, HomeState>(
                          bloc: homeBloc,
                          builder: (context, state) {
                            return Row(
                              children: <Widget>[
                                homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                    ? Text(
                                        snapshot.data[index].name,
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                        ),
                                      )
                                    : Text(snapshot.data[index].name),
                                SizedBox(
                                  width: 10 * settingsBloc.screenSizeRelation,
                                ),
                                snapshot.data[index].isPrimary
                                    ? SvgPicture.asset('assets/images/small_star.svg')
                                    : Container(),
                              ],
                            );
                          },
                        ),
                        subtitle: BlocEventStateBuilder<HomeEvent, HomeState>(
                          bloc: homeBloc,
                          builder: (context, state) {
                            return homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                ? Text(
                                    snapshot.data[index].frequency,
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark.withOpacity(0.3),
                                    ),
                                  )
                                : Text(snapshot.data[index].frequency);
                          },
                        ),
                        trailing: BlocEventStateBuilder<HomeEvent, HomeState>(
                          bloc: homeBloc,
                          builder: (context, state) {
                            return Container(
                              height: 40 * settingsBloc.screenSizeRelation,
                              width: 47 * settingsBloc.screenSizeRelation,
                              decoration: BoxDecoration(
                                color: homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                    ? Theme.of(context).accentColor
                                    : settingsBloc.theme == 'White theme'
                                        ? Theme.of(context).primaryColorLight
                                        : Color.fromRGBO(46, 46, 46, 1),
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(4),
                                  bottomLeft: Radius.circular(4),
                                ),
                              ),
                              child: Icon(
                                Icons.check,
                                color: homeBloc.dailyHabitsIsDone[snapshot.data[index].id]
                                    ? Theme.of(context).primaryColorLight
                                    : Theme.of(context).primaryColorDark,
                                size: 15 * settingsBloc.screenSizeRelation,
                              ),
                            );
                          },
                        ),
                      ),
                      onTap: () async {
                        await homeBloc.changeIsDone(snapshot.data[index].id);
                        homeBloc.emitEvent(HomeEventChangeIsDone());
                        await homeBloc.setTodaysHabits();
                      },
                    );
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
