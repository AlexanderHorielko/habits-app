import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:intl/intl.dart';

class Navbar extends StatelessWidget {
  final String tabName;
  final String title;
  final IconButton iconButton;
  final Color textColor;

  Navbar(this.tabName, {this.title, this.iconButton, this.textColor});

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    BottomNavbarBloc bottomNabvarBloc = BlocProvider.of<BottomNavbarBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return Container(
      padding: EdgeInsets.only(
        top: 54 * settingsBloc.screenSizeRelation,
      ),
      child: Container(
        margin: EdgeInsets.only(
          left: 16 * settingsBloc.screenSizeRelation,
          right: 8 * settingsBloc.screenSizeRelation,
        ),
        child: Row(
          children: <Widget>[
            Text(
              title == null ? DateFormat('EEEE').format(DateTime.now()) : title,
              style: TextStyle(
                fontSize: 36 * settingsBloc.screenSizeRelation,
                color: textColor != null ? textColor : Theme.of(context).primaryColorDark,
              ),
            ),
            Spacer(),
            iconButton == null
                ? IconButton(
                    icon: SvgPicture.asset(
                      'assets/images/settings.svg',
                      color: textColor != null ? textColor : Theme.of(context).primaryColorDark,
                    ),
                    onPressed: () {
                      homeBloc.previousTabName = tabName;
                      bottomNabvarBloc.emitEvent(BottomNavbarEventChangeTab('Settings'));
                    },
                  )
                : iconButton,
          ],
        ),
      ),
    );
  }
}
