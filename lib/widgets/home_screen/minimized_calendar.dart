import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/screens/calendar_screen.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

class MinimizedCalendar extends StatefulWidget {
  const MinimizedCalendar({Key key}) : super(key: key);

  @override
  _MinimizedCalendarState createState() => _MinimizedCalendarState();
}

class _MinimizedCalendarState extends State<MinimizedCalendar> {
  CalendarController _calendarController;
  double startDragPosition = 0;
  double offset = 0;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  StartingDayOfWeek getWeekDay() {
    if (DateFormat('EEEE').format(DateTime.now()) == 'Monday') {
      return StartingDayOfWeek.monday;
    } else if (DateFormat('EEEE').format(DateTime.now()) == 'Tuesday') {
      return StartingDayOfWeek.tuesday;
    } else if (DateFormat('EEEE').format(DateTime.now()) == 'Wednesday') {
      return StartingDayOfWeek.wednesday;
    } else if (DateFormat('EEEE').format(DateTime.now()) == 'Thursday') {
      return StartingDayOfWeek.thursday;
    } else if (DateFormat('EEEE').format(DateTime.now()) == 'Friday') {
      return StartingDayOfWeek.friday;
    } else if (DateFormat('EEEE').format(DateTime.now()) == 'Saturday') {
      return StartingDayOfWeek.saturday;
    } else
      return StartingDayOfWeek.sunday;
  }

  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);

    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 24 * settingsBloc.screenSizeRelation,
            left: 10 * settingsBloc.screenSizeRelation,
            right: 10 * settingsBloc.screenSizeRelation,
          ),
          width: double.infinity,
          child: TableCalendar(
            initialCalendarFormat: CalendarFormat.week,
            startingDayOfWeek: getWeekDay(),
            availableCalendarFormats: {
              CalendarFormat.week: 'Week',
            },
            calendarController: _calendarController,
            calendarStyle: CalendarStyle(
              contentPadding: EdgeInsets.only(bottom: 22 * settingsBloc.screenSizeRelation),
              renderDaysOfWeek: false,
            ),
            headerVisible: false,
            builders: CalendarBuilders(
              dayBuilder: (context, date, events) {
                final curDateTime = DateTime.now();
                final currentDate = DateTime(
                  curDateTime.year,
                  curDateTime.month,
                  curDateTime.day,
                );

                final calendarDate = DateTime(date.year, date.month, date.day);

                return Container(
                  child: Center(
                    child: Container(
                      width: 45 * settingsBloc.screenSizeRelation,
                      height: 55 * settingsBloc.screenSizeRelation,
                      decoration: BoxDecoration(
                        color: currentDate == calendarDate
                            ? Theme.of(context).accentColor
                            : settingsBloc.theme == 'White theme'
                                ? Theme.of(context).primaryColorLight
                                : Color.fromRGBO(46, 46, 46, 1),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            date.day.toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 14 * settingsBloc.screenSizeRelation,
                              color: currentDate == calendarDate
                                  ? Theme.of(context).primaryColorLight
                                  : Theme.of(context).primaryColorDark,
                            ),
                          ),
                          Text(
                            DateFormat('MMM').format(date).toLowerCase(),
                            style: TextStyle(
                              fontSize: 12,
                              color: currentDate == calendarDate
                                  ? Theme.of(context).primaryColorLight
                                  : Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        GestureDetector(
          onVerticalDragStart: (details) {
            startDragPosition = details.globalPosition.dy;
          },
          onVerticalDragUpdate: (details) {
            offset = details.globalPosition.dy - startDragPosition;
          },
          onVerticalDragEnd: (details) {
            if (offset > 100) {
              bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Calendar'));
            }
          },
          child: Container(
            width: 25 * settingsBloc.screenSizeRelation,
            height: 3 * settingsBloc.screenSizeRelation,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Theme.of(context).primaryColorDark.withOpacity(0.1),
            ),
          ),
        ),
      ],
    );
  }
}
