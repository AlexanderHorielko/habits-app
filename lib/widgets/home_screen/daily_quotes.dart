import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';

class DailyQuotes extends StatelessWidget {
  const DailyQuotes({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Container(
      margin: EdgeInsets.only(top: 35 * settingsBloc.screenSizeRelation),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 15 * settingsBloc.screenSizeRelation),
            child: Row(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Daily ',
                      style: TextStyle(
                        fontSize: 16 * settingsBloc.screenSizeRelation,
                      ),
                    ),
                    Text(
                      'quotes',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16 * settingsBloc.screenSizeRelation,
                      ),
                    )
                  ],
                ),
                Spacer(),
                IconButton(
                  icon: Icon(
                    Icons.more_vert,
                    size: 20 * settingsBloc.screenSizeRelation,
                  ),
                  onPressed: () {},
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 15 * settingsBloc.screenSizeRelation),
                width: MediaQuery.of(context).size.width * 0.67,
                child: Text(
                  'Push yourself, because no one else is going to do it for you.',
                  style: TextStyle(
                    fontSize: 14 * settingsBloc.screenSizeRelation,
                    color: Theme.of(context).primaryColorDark.withOpacity(0.8),
                  ),
                ),
              ),
              Container(
                width: 70 * settingsBloc.screenSizeRelation,
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(
              top: 15 * settingsBloc.screenSizeRelation,
            ),
            width: MediaQuery.of(context).size.width * 0.92,
            child: Divider(),
          ),
        ],
      ),
    );
  }
}
