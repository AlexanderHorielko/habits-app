import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/screens/add_habit_screen.dart';
import 'package:habit/screens/home_screen.dart';

class BottomNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            child: Align(
              child: Container(
                width: 74 * settingsBloc.screenSizeRelation,
                height: 74 * settingsBloc.screenSizeRelation,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [0.5, 1],
                    colors: settingsBloc.theme == 'White theme'
                        ? [Theme.of(context).primaryColorLight, Color.fromRGBO(254, 234, 230, 1)]
                        : [Color.fromRGBO(46, 46, 46, 1), Color.fromRGBO(47, 67, 71, 1)],
                  ),
                  color: Color.fromRGBO(254, 234, 230, 1),
                  shape: BoxShape.circle,
                ),
                alignment: Alignment.center,
                child: Container(
                  width: 54.0 * settingsBloc.screenSizeRelation,
                  height: 54.0 * settingsBloc.screenSizeRelation,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    shape: BoxShape.circle,
                  ),
                  alignment: Alignment.center,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                    ),
                    child: IconButton(
                      icon: Icon(
                        Icons.add,
                        color: Theme.of(context).primaryColorLight,
                      ),
                      color: Theme.of(context).accentColor,
                      onPressed: () async {
                        await Navigator.of(context).pushNamed(AddHabitScreen.routeName);
                        Navigator.of(context).pushNamed(HomeScreen.routeName);
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
          BlocEventStateBuilder(
            bloc: bottomNavbarBloc,
            builder: (context, state) {
              return Align(
                child: Container(
                  margin: EdgeInsets.only(top: 40 * settingsBloc.screenSizeRelation),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      state.currentTab == 'Daily'
                          ? Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: Image.asset(
                                  'assets/images/sun_active.png',
                                ),
                                onPressed: () {},
                              ),
                            )
                          : Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: SvgPicture.asset(
                                  'assets/images/empty_sun.svg',
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                onPressed: () {
                                  bottomNavbarBloc.emitEvent(
                                    BottomNavbarEventChangeTab('Daily'),
                                  );
                                },
                              ),
                            ),
                      state.currentTab == 'Primary'
                          ? Container(
                              margin: EdgeInsets.only(
                                right: 80 * settingsBloc.screenSizeRelation,
                              ),
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: SvgPicture.asset(
                                  'assets/images/big_star_active.svg',
                                ),
                                onPressed: () {},
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(
                                right: 80 * settingsBloc.screenSizeRelation,
                              ),
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: SvgPicture.asset(
                                  'assets/images/big_star.svg',
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                onPressed: () {
                                  bottomNavbarBloc.emitEvent(
                                    BottomNavbarEventChangeTab('Primary'),
                                  );
                                },
                              ),
                            ),
                      state.currentTab == 'HabitsGroup'
                          ? Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: Image.asset(
                                  'assets/images/settings_bottom_active.png',
                                ),
                                onPressed: () {},
                              ),
                            )
                          : Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: Image.asset(
                                  'assets/images/settings_bottom.png',
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                onPressed: () {
                                  bottomNavbarBloc.emitEvent(
                                    BottomNavbarEventChangeTab('HabitsGroup'),
                                  );
                                },
                              ),
                            ),
                      state.currentTab == 'Progress'
                          ? Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: SvgPicture.asset(
                                  'assets/images/progress_active.svg',
                                ),
                                onPressed: () {},
                              ),
                            )
                          : Container(
                              child: IconButton(
                                iconSize: 40 * settingsBloc.screenSizeRelation,
                                icon: SvgPicture.asset(
                                  'assets/images/progress.svg',
                                  color: Theme.of(context).primaryColorDark,
                                ),
                                onPressed: () {
                                  bottomNavbarBloc.emitEvent(
                                    BottomNavbarEventChangeTab('Progress'),
                                  );
                                },
                              ),
                            ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
