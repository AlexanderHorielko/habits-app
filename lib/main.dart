import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_event.dart';
import 'package:habit/bloc/settings_bloc/settings_state.dart';
import 'package:habit/screens/add_group_screen.dart';
import 'package:habit/screens/add_habit_screen.dart';
import 'package:habit/screens/calendar_screen.dart';
import 'package:habit/screens/habits_groups_screen.dart';
import 'package:habit/screens/habits_screen.dart';
import 'package:habit/screens/home_screen.dart';
import 'package:habit/screens/primary_habits_screen.dart';
import 'package:habit/screens/settings_screen.dart';

import 'bloc/base/bloc_event_state_builder.dart';
import 'bloc/auth_bloc/auth_bloc.dart';
import 'bloc/auth_bloc/auth_event.dart';
import 'bloc/auth_bloc/auth_state.dart';
import 'helpers/helper_functions.dart';
import 'screens/auth_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SettingsBloc _settingsBloc = SettingsBloc();
  final allSettings = await _settingsBloc.getSettings();
  _settingsBloc.emitEvent(SettingsEventSetSettings(allSettings));
  AuthBloc _authBloc = AuthBloc();

  _authBloc.emitEvent(AuthEventAppStarted());

  runApp(MyApp(_settingsBloc, _authBloc));
}

class MyApp extends StatefulWidget {
  static const double defaultHeight = 812;
  final SettingsBloc allSettings;
  final AuthBloc _authBloc;

  MyApp(this.allSettings, this._authBloc);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AuthState oldAuthenticationState;
  HomeBloc _homeBloc = HomeBloc();
  BottomNavbarBloc _bottomNavbarBloc = BottomNavbarBloc();

  @override
  Widget build(BuildContext context) {
    // return MultiBlocProvider(
    //   blocBuilders: [
    //     BlocProvider<AuthBloc>(blocBuilder: () => widget._authBloc),
    //     BlocProvider<BottomNavbarBloc>(blocBuilder: () => _bottomNavbarBloc),
    //     BlocProvider<HomeBloc>(blocBuilder: () => _homeBloc),
    //     BlocProvider<SettingsBloc>(blocBuilder: () => widget.allSettings),
    //   ],
    //   child: BlocEventStateBuilder<SettingsEvent, SettingsState>(
    //     bloc: widget.allSettings,
    //     builder: (context, state) {
    //       if (widget.allSettings.settingsWereChanged == true) {
    //         _bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Settings'));
    //       } else {
    //         _bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Daily'));
    //       }

    //       return MaterialApp(
    //         title: 'BLoC Samples',
    //         theme: ThemeData(
    //           brightness: widget.allSettings.theme == 'White theme' ? Brightness.light : Brightness.dark,
    //           primaryColorDark: widget.allSettings.theme == 'White theme'
    //               ? Color.fromRGBO(40, 42, 52, 1)
    //               : Color.fromRGBO(250, 250, 250, 1),
    //           primaryColorLight: widget.allSettings.theme == 'White theme'
    //               ? Color.fromRGBO(250, 250, 250, 1)
    //               : Color.fromRGBO(40, 42, 52, 1),
    //           primarySwatch: Colors.blue,
    //           accentColor: Color.fromRGBO(245, 112, 58, 1),
    //           highlightColor: Color.fromRGBO(255, 242, 208, 1),
    //           fontFamily: 'CircularStd',
    //         ),
    //         home: BlocEventStateBuilder<AuthEvent, AuthState>(
    //           bloc: widget._authBloc,
    //           builder: (BuildContext context, AuthState state) {
    //             if (state.isAuthenticated) {
    //               HelperFunctions.redirectToPage(context, HomeScreen());
    //             } else if (state.isAuthenticating || state.hasFailed) {
    //               return Center(
    //                 child: CircularProgressIndicator(),
    //               );
    //             } else {
    //               HelperFunctions.redirectToPage(context, AuthScreen());
    //             }
    //             return Container();
    //           },
    //         ),
    //         routes: {
    //           CalendarScreen.routeName: (context) => CalendarScreen(),
    //           HomeScreen.routeName: (context) => HomeScreen(),
    //           SettingsScreen.routeName: (context) => SettingsScreen(),
    //           AddHabitScreen.routeName: (context) => AddHabitScreen(),
    //           AddGroupScreen.routeName: (context) => AddGroupScreen(),
    //           HabitsGroupsScreen.routeName: (context) => HabitsGroupsScreen(),
    //           HabitsScreen.routeName: (context) => HabitsScreen(),
    //           PrimaryHabitsScreen.routeName: (context) => PrimaryHabitsScreen(),
    //         },
    //       );
    //     },
    //   ),
    // );

    return BlocProvider<AuthBloc>(
      blocBuilder: () => widget._authBloc,
      child: BlocProvider<BottomNavbarBloc>(
        blocBuilder: () => _bottomNavbarBloc,
        child: BlocProvider<HomeBloc>(
          blocBuilder: () => _homeBloc,
          child: BlocProvider<SettingsBloc>(
            blocBuilder: () => widget.allSettings,
            child: BlocEventStateBuilder<SettingsEvent, SettingsState>(
              bloc: widget.allSettings,
              builder: (context, state) {
                // widget._authBloc.emitEvent(AuthEventLogout());
                if (widget.allSettings.settingsWereChanged == true) {
                  _bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Settings'));
                } else {
                  _bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Daily'));
                }

                return MaterialApp(
                  title: 'BLoC Samples',
                  theme: ThemeData(
                    brightness: widget.allSettings.theme == 'White theme' ? Brightness.light : Brightness.dark,
                    primaryColorDark: widget.allSettings.theme == 'White theme'
                        ? Color.fromRGBO(40, 42, 52, 1)
                        : Color.fromRGBO(250, 250, 250, 1),
                    primaryColorLight: widget.allSettings.theme == 'White theme'
                        ? Color.fromRGBO(250, 250, 250, 1)
                        : Color.fromRGBO(40, 42, 52, 1),
                    primarySwatch: Colors.blue,
                    accentColor: Color.fromRGBO(245, 112, 58, 1),
                    highlightColor: Color.fromRGBO(255, 242, 208, 1),
                    fontFamily: 'CircularStd',
                  ),
                  home: BlocEventStateBuilder<AuthEvent, AuthState>(
                    bloc: widget._authBloc,
                    builder: (BuildContext context, AuthState state) {
                      if (state.isAuthenticated) {
                        HelperFunctions.redirectToPage(context, HomeScreen());
                      } else if (state.isAuthenticating || state.hasFailed) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        HelperFunctions.redirectToPage(context, AuthScreen());
                      }
                      return Container();
                    },
                  ),
                  routes: {
                    CalendarScreen.routeName: (context) => CalendarScreen(),
                    HomeScreen.routeName: (context) => HomeScreen(),
                    SettingsScreen.routeName: (context) => SettingsScreen(),
                    AddHabitScreen.routeName: (context) => AddHabitScreen(),
                    AddGroupScreen.routeName: (context) => AddGroupScreen(),
                    HabitsGroupsScreen.routeName: (context) => HabitsGroupsScreen(),
                    HabitsScreen.routeName: (context) => HabitsScreen(),
                    PrimaryHabitsScreen.routeName: (context) => PrimaryHabitsScreen(),
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
