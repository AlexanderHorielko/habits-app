import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/firebase_auth_repository.dart';

import 'habits_group_repository.dart';

class HabitRepository {
  static Future<FirebaseUser> _getCurrentUser() async {
    return await FirebaseAuthRepository.getCurrentUser();
  }

  static Future<QuerySnapshot> _getHabits() async {
    FirebaseUser user = await FirebaseAuthRepository.getCurrentUser();
    final allDocuments = await Firestore.instance.collection('users/${user.uid}/habits').getDocuments();
    return allDocuments;
  }

  static Stream<QuerySnapshot> getHabitsStream(FirebaseUser currUser) {
    final allDocuments = Firestore.instance.collection('users/${currUser.uid}/habits').snapshots();
    return allDocuments;
  }

  static Future<List<Habit>> listOfHabits() async {
    final habits = await _getHabits();
    final listOfHabits = transformToHabitsList(habits);
    return listOfHabits;
  }

  static Habit transformToHabit(DocumentSnapshot habit) {
    return Habit(
      id: habit.documentID,
      name: habit.data['name'],
      habitGroup: habit.data['habitGroup'],
      frequency: habit.data['frequency'],
      isPrimary: habit.data['isPrimary'],
      remindHabits: habit.data['remindHabits'],
      isDone: habit.data['isDone'],
      remindTime: DateTime.fromMillisecondsSinceEpoch(
        habit.data['remindTime'].seconds * 1000,
      ),
      created: DateTime.fromMillisecondsSinceEpoch(
        habit.data['created'].seconds * 1000,
      ),
    );
  }

  static List<Habit> transformToHabitsList(QuerySnapshot data) {
    final listOfHabits = data.documents.map((e) {
      return transformToHabit(e);
    }).toList();
    return listOfHabits;
  }

  static Future<void> addHabit(Habit habit, {String id}) async {
    final user = await _getCurrentUser();
    DocumentReference habitId;
    if (id != null) {
      await Firestore.instance.document('users/${user.uid}/habits/$id').setData(habitToJson(habit));
    } else {
      habitId = await Firestore.instance.collection('users/${user.uid}/habits').add(habitToJson(habit));
      final allDays = await DaysRepository.getAllDays();
      allDays.documents.forEach((element) async {
        final elementDate = HelperFunctions.getDateTimeFromTimestamp(element.data['date']);
        final today = DateTime(
          DateTime.now().year,
          DateTime.now().month,
          DateTime.now().day,
        );
        if (elementDate == today) {
          final todayHabits = element.data['habits'];
          todayHabits[habitId.documentID] = false;
          await Firestore.instance.document('users/${user.uid}/days/${element.documentID}').updateData({
            'habits': todayHabits,
          });
          return Future;
        }
      });
    }
  }

  static Future<QuerySnapshot> deleteHabit(String habitId) async {
    final user = await _getCurrentUser();
    final days = await DaysRepository.getAllDays();
    await Firestore.instance.document('users/${user.uid}/habits/$habitId').delete();

    final oldDays = days;
    Map<String, dynamic> dayHabits = {};
    days.documents.forEach((day) {
      dayHabits = day.data['habits'];
      day.data['habits'].forEach((key, value) async {
        if (habitId == key) {
          dayHabits.remove(key);
          await Firestore.instance.document('users/${user.uid}/days/${day.documentID}').updateData({
            'habits': dayHabits,
          });
          return oldDays;
        }
      });
    });
    return oldDays;
  }

  static Future<void> deleteHabitFromGroup(List<dynamic> habits) async {
    final user = await _getCurrentUser();
    final days = await DaysRepository.getAllDays();

    days.documents.forEach((day) async {
      final dayHabits = day.data['habits'];

      habits.forEach((habit) async {
        if (day.data['habits'].containsKey(habit.id)) {
          dayHabits.remove(habit.id);
        }

        await Firestore.instance.document('users/${user.uid}/habits/${habit.id}').delete();
      });

      await Firestore.instance.document('users/${user.uid}/days/${day.documentID}').updateData({
        'habits': dayHabits,
      });
    });
  }

  static Future<DocumentSnapshot> getHints() async {
    final user = await _getCurrentUser();

    return await Firestore.instance.document('users/${user.uid}/habitsHints/Hints').get();
  }

  static Stream<QuerySnapshot> habitsStream(FirebaseUser user) {
    return Firestore.instance.collection('users/${user.uid}/habits').snapshots();
  }

  static Future<void> changeIsDone(String habitId, {bool isHabitDone = true}) async {
    final user = await _getCurrentUser();
    if (!isHabitDone) {
      await Firestore.instance.document('users/${user.uid}/habits/$habitId').updateData(
        {
          'isDone': false,
        },
      );
    } else {
      final habit = await Firestore.instance.document('users/${user.uid}/habits/$habitId').get();

      await Firestore.instance.document('users/${user.uid}/habits/$habitId').updateData(
        {
          'isDone': !habit['isDone'],
        },
      );
    }
  }

  static Map<String, dynamic> habitToJson(Habit habit) {
    return {
      'name': habit.name,
      'habitGroup': habit.habitGroup,
      'frequency': habit.frequency,
      'isPrimary': habit.isPrimary,
      'remindHabits': habit.remindHabits,
      'isDone': habit.isDone,
      'remindTime': Timestamp.fromDate(habit.remindTime),
      'created': Timestamp.fromDate(habit.created),
    };
  }

  static Map<String, dynamic> habitToMap(Habit habit) {
    return {
      'name': habit.name,
      'habitGroup': habit.habitGroup,
      'frequency': habit.frequency,
      'isPrimary': habit.isPrimary,
      'remindHabits': habit.remindHabits,
      'isDone': habit.isDone,
      'remindTime': habit.remindTime,
      'created': habit.created,
    };
  }

  static Future<Map<String, dynamic>> getAddHabitData() async {
    final hints = await HabitRepository.getHints();
    final groups = await HabitsGroupRepository.getAllGroups();
    return {
      'hints': hints,
      'groups': groups,
    };
  }
}
