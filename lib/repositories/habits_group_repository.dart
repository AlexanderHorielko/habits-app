import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:habit/models/habit_group.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/habit_repository.dart';

import 'firebase_auth_repository.dart';

class HabitsGroupRepository {
  static Future<QuerySnapshot> getAllGroups() async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    return await Firestore.instance
        .collection('/users/${user.uid}/habitsGroups')
        .getDocuments();
  }

  static Future<Map<String, dynamic>> getGroup(String groupName) async {
    final firebaseGroups = await getAllGroups();
    List<DocumentSnapshot> groups = firebaseGroups.documents;
    Map<String, dynamic> group = {};

    for (int i = 0; i < groups.length; i++) {
      if (groups[i].data != null && groups[i].data['name'] == groupName) {
        group = groups[i].data;
        group['id'] = groups[i].documentID;
        break;
      }
    }

    return group;
  }

  static Map<String, dynamic> groupToMap(HabitGroup habitGroup) {
    return {
      'name': habitGroup.name,
      'color': habitGroup.colorValues,
    };
  }

  static Future<void> addGroup(HabitGroup habitGroup) async {
    final user = await FirebaseAuthRepository.getCurrentUser();

    final group = await getGroup(habitGroup.name);

    if (group['name'] == null) {
      await Firestore.instance
          .collection('/users/${user.uid}/habitsGroups')
          .add(
            groupToMap(habitGroup),
          );
    } else {
      throw Exception('Group already exists!!!');
    }
  }

  static Future<void> updateGroup(HabitGroup habitGroup, String oldName) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    final group = await getGroup(oldName);
    final habits = await HabitRepository.listOfHabits();

    if (group['name'] != null) {
      await Firestore.instance
          .document('/users/${user.uid}/habitsGroups/${group['id']}')
          .updateData(
            groupToMap(habitGroup),
          );
      habits.forEach(
        (element) async {
          if (element.habitGroup == oldName) {
            await Firestore.instance
                .document('/users/${user.uid}/habits/${element.id}')
                .updateData({
              'habitGroup': habitGroup.name,
            });
          }
        },
      );
    } else {
      throw Exception('Can`t get group!!!');
    }
  }

  static Stream<QuerySnapshot> getGroupsStream(FirebaseUser currUser) {
    return Firestore.instance
        .collection('/users/${currUser.uid}/habitsGroups')
        .snapshots();
  }

  static Future<QuerySnapshot> deleteGroup(
      String groupId, List<dynamic> habits) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    final oldDays = await DaysRepository.getAllDays();

    await Firestore.instance
        .document('users/${user.uid}/habitsGroups/$groupId')
        .delete();

    await HabitRepository.deleteHabitFromGroup(habits);

    return oldDays;
  }
}
