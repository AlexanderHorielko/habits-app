import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:habit/repositories/firebase_auth_repository.dart';

class SettingsRepository {
  static Future<Map<String, dynamic>> getSettingsData() async {
    final user = await FirebaseAuth.instance.currentUser();

    if (user == null) {
      return {
        'inspirationText': false,
        'language': 'English',
        'notification': false,
        'theme': 'White theme',
      };
    }

    final res = await Firestore.instance.document('users/${user.uid}/settings/Settings').get();

    return res.data;
  }

  static Future<void> sendNotificationSwitch(bool notificationOn) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    await Firestore.instance
        .document('users/${user.uid}/settings/Settings')
        .updateData({'notification': notificationOn});
  }

  static Future<void> sendInspirationTextSwitch(bool inpirationTextOn) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    await Firestore.instance
        .document('users/${user.uid}/settings/Settings')
        .updateData({'inspirationText': inpirationTextOn});
  }

  static Future<void> sendLanguage(String lang) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    await Firestore.instance.document('users/${user.uid}/settings/Settings').updateData({'language': lang});
  }

  static Future<void> sendTheme(String theme) async {
    final user = await FirebaseAuthRepository.getCurrentUser();
    await Firestore.instance.document('users/${user.uid}/settings/Settings').updateData({'theme': theme});
  }
}
