import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/firebase_auth_repository.dart';

class DaysRepository {
  static Future<QuerySnapshot> getAllDays() async {
    final user = await FirebaseAuthRepository.getCurrentUser();

    final days = await Firestore.instance.collection('users/${user.uid}/days').getDocuments();

    return days;
  }

  static Future<void> setDays(List<Habit> habits) async {
    final user = await FirebaseAuthRepository.getCurrentUser();

    Map<String, dynamic> allHabits = {};
    habits.forEach((element) {
      allHabits[element.id] = element.isDone;
    });

    final DateTime dateKey = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);

    final today = await getDay(dateKey);

    await Firestore.instance.document('users/${user.uid}/days/${today.documentID}').updateData({'habits': allHabits});
  }

  static Future<DocumentSnapshot> getDay(DateTime dateKey) async {
    final days = await getAllDays();

    DocumentSnapshot day;
    days.documents.forEach((element) {
      if (HelperFunctions.getDateTimeFromTimestamp(element['date']) == dateKey) {
        day = element;
      }
    });
    return day;
  }

  static Future<void> createTodayDocument(BuildContext context) async {
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    if (!homeBloc.todayDocumentCreated) {
      DateTime dateKey = DateTime(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day,
      );
      final user = await FirebaseAuthRepository.getCurrentUser();
      await Firestore.instance.collection('users/${user.uid}/days').add({'date': dateKey, 'habits': {}});
      homeBloc.todayDocumentCreated = true;
      await homeBloc.setTodaysHabits(initialDaySetup: true);
    }
  }

  static Future<void> resetHabitIntoDays(QuerySnapshot oldDays) async {
    final user = await FirebaseAuthRepository.getCurrentUser();

    oldDays.documents.forEach((element) async {
      await Firestore.instance.document('users/${user.uid}/days/${element.documentID}').updateData({
        'habits': element.data['habits'],
      });
    });
  }
}
