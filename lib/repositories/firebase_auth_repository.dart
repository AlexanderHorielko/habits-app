import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';

class FirebaseAuthRepository {
  static FirebaseAuth getFirebaseAuthInstance() {
    return FirebaseAuth.instance;
  }

  static Future<FirebaseUser> getCurrentUser() async {
    return await FirebaseAuth.instance.currentUser();
  }

  static Future<AuthResult> signIn(String email, String password) async {
    try {
      AuthResult result = await getFirebaseAuthInstance().signInWithEmailAndPassword(email: email, password: password);
      return result;
    } catch (error) {
      throw error;
    }
  }

  static Future<AuthResult> createUser({String email, String password, bool signUpWithGoogle = false}) async {
    AuthResult result;
    try {
      if (signUpWithGoogle) {
        final GoogleSignIn googleSignIn = GoogleSignIn();
        final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
        final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );
        result = await getFirebaseAuthInstance().signInWithCredential(credential);
      } else {
        result = await getFirebaseAuthInstance().createUserWithEmailAndPassword(email: email, password: password);
      }

      await Firestore.instance.collection('users').document('${result.user.uid}').setData({'email': result.user.email});

      await Firestore.instance
          .document('users/${result.user.uid}/settings/Settings')
          .setData({'inspirationText': false, 'language': 'English', 'notification': false, 'theme': 'White theme'});
      await Firestore.instance.document('users/${result.user.uid}/habitsHints/Hints').setData({
        'hints': [
          'Meditation',
          'Read',
          'Draw',
          'Run',
          'English',
        ]
      });

      return result;
    } catch (error) {
      throw error;
    }
  }

  static Future<void> signOut() async {
    await getFirebaseAuthInstance().signOut();
  }

  static Future<void> resetPassword(AuthBloc authBloc, BuildContext context) async {
    FocusScope.of(context).unfocus();
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: authBloc.user.email);
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(
          context,
          'Email was successfully sent!',
          color: Theme.of(context).highlightColor,
        ),
      );
    } catch (e) {
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(
          context,
          e.message,
          isError: true,
        ),
      );
    }
  }
}
