import 'package:habit/models/habit.dart';
import 'package:habit/repositories/habit_repository.dart';

class PrimaryHabitsRepository {
  static Future<List<Habit>> getPrimaryHabits() async {
    final habits = await HabitRepository.listOfHabits();

    List<Habit> primaryHabits = [];

    habits.forEach((element) {
      if (element.isPrimary) {
        primaryHabits.add(element);
      }
    });

    return primaryHabits;
  }
}
