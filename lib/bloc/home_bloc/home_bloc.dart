import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/home_bloc/home_event.dart';
import 'package:habit/bloc/home_bloc/home_state.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/habit_repository.dart';
import 'package:habit/repositories/habits_group_repository.dart';

class HomeBloc extends BlocEventStateBase<HomeEvent, HomeState> {
  String _currentTab = 'Daily';
  Map<String, bool> dailyHabitsIsDone = {};
  Map<String, dynamic> arguments = {};
  String previousTabName = '';
  bool todayDocumentCreated = false;

  HomeBloc()
      : super(
          initialState: HomeState.initialState(),
        );

  Stream<HomeState> eventHandler(HomeEvent event, HomeState currentState) async* {
    try {
      if (event is HomeEventTimerChange) {
        await Future.delayed(const Duration(seconds: 1));
        yield HomeState.changedTimer(_currentTab);
      } else if (event is HomeEventAddHabit) {
        yield HomeState.loading(_currentTab);
      } else if (event is HomeEventGetAllHabits) {
        yield HomeState.loading(_currentTab, getHabits: true);
      } else if (event is HomeEventChangeIsDone) {
        yield HomeState.changeIsDone(dailyHabitsIsDone, _currentTab);
      }
    } catch (e) {
      yield HomeState.error(e.message);
    }
  }

  Future<void> addHabit(Habit habit) async {
    await HabitRepository.addHabit(habit);
  }

  Future<void> getGroup(String groupName) async {
    return await HabitsGroupRepository.getGroup(groupName);
  }

  Future<List<Habit>> getTodayHabits(BuildContext context) async {
    QuerySnapshot days = await DaysRepository.getAllDays();
    DocumentSnapshot today;

    DateTime todayTime = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    );

    days.documents.forEach((element) {
      final date = DateTime.fromMillisecondsSinceEpoch(element.data['date'].seconds * 1000);
      if (date == todayTime) {
        today = element;
      }
    });
    if (today == null) {
      await DaysRepository.createTodayDocument(context);
    }

    List<Habit> todayHabits = await listOfTodayHabits();

    return todayHabits;
  }

  Future<List<Habit>> listOfTodayHabits() async {
    List<Habit> habits = await HabitRepository.listOfHabits();

    DateTime todayTime = DateTime(
      DateTime.now().year,
      DateTime.now().month,
      DateTime.now().day,
    );

    final List<Habit> todayHabits = [];

    habits.forEach(
      (element) {
        final difference = element.created.difference(todayTime).inDays;
        int frequency;

        switch (element.frequency) {
          case 'Every day':
            frequency = 0;
            break;
          case 'In a day':
            frequency = 2;
            break;
          case 'In 2 days':
            frequency = 3;
            break;
          case 'Once a week':
            frequency = 7;
            break;
          default:
        }

        final show = frequency == 0 ? 0 : difference % frequency;

        if (show == 0) {
          todayHabits.add(element);
        }
      },
    );

    return todayHabits;
  }

  Future<void> setTodaysHabits({bool initialDaySetup}) async {
    final todayHabits = await listOfTodayHabits();

    if (initialDaySetup != null) {
      todayHabits.forEach((element) async {
        element.isDone = false;
        await changeIsDone(element.id, isHabitDone: false);
      });
    }
    await DaysRepository.setDays(todayHabits);
  }

  Stream<QuerySnapshot> habitsStream(FirebaseUser user) {
    return HabitRepository.habitsStream(user);
  }

  Future<void> changeIsDone(String habitId, {bool isHabitDone = true}) async {
    if (!isHabitDone) {
      dailyHabitsIsDone[habitId] = false;
    } else {
      dailyHabitsIsDone[habitId] = !dailyHabitsIsDone[habitId];
    }
    return await HabitRepository.changeIsDone(habitId, isHabitDone: isHabitDone);
  }
}
