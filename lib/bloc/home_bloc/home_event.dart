import 'package:habit/bloc/base/bloc_event_state_base.dart';

abstract class HomeEvent extends BlocEvent {}

class HomeEventTimerChange extends HomeEvent {}

class HomeEventAddHabit extends HomeEvent {}

class HomeEventGetAllHabits extends HomeEvent {}

class HomeEventChangeIsDone extends HomeEvent {}

