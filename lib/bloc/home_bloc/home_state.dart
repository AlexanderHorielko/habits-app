import 'package:habit/bloc/base/bloc_event_state_base.dart';

class HomeState extends BlocState {
  bool loading = false;
  DateTime time = DateTime.now();
  String currentTab = 'Daily';
  final String error;
  bool addHabit = false;
  bool getHabits = false;
  Map<String, bool> changeIsDone = {};

  HomeState({
    this.time,
    this.error,
    this.currentTab,
    this.loading,
    this.addHabit,
    this.getHabits,
  });

  factory HomeState.initialState() {
    return HomeState(
      time: DateTime.now(),
      currentTab: 'Daily',
      getHabits: true,
    );
  }

  factory HomeState.loading(String tabName, {bool addHabit, bool getHabits}) {
    return HomeState(
      loading: true,
      time: DateTime.now(),
      currentTab: tabName,
      addHabit: addHabit,
      getHabits: getHabits,
    );
  }

  factory HomeState.changedTimer(String tabName) {
    return HomeState(time: DateTime.now(), currentTab: tabName);
  }

  factory HomeState.error(String message) {
    return HomeState(error: message);
  }

  factory HomeState.changeIsDone(Map<String, bool> change, String tabName) {
    return HomeState(
      time: DateTime.now(),
      currentTab: tabName,
    );
  }
}
