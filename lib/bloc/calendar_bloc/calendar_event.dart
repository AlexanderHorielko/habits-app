import '../base/bloc_event_state_base.dart';

abstract class CalendarEvent extends BlocEvent {}

class CalendarEventShowHabits extends CalendarEvent {}
