import 'package:habit/bloc/base/bloc_event_state_base.dart';

class CalendarState extends BlocState {
  DateTime choosedDate;
  final bool showHabits;

  CalendarState({this.showHabits, this.choosedDate});

  factory CalendarState.showHabits(DateTime date) {
    return CalendarState(showHabits: true, choosedDate: date);
  }
}
