import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/calendar_bloc/calendar_event.dart';
import 'package:habit/bloc/calendar_bloc/calendar_state.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/habit_repository.dart';

class CalendarBloc extends BlocEventStateBase<CalendarEvent, CalendarState> {
  final DateTime todayKey = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  Map<DateTime, List<dynamic>> habits;
  Map<DateTime, List<Map<String, dynamic>>> habitsMaps = {};
  List<DateTime> streakDates = [];

  DateTime choosedDate;

  CalendarBloc()
      : super(
          initialState: CalendarState.showHabits(
            DateTime(
              DateTime.now().year,
              DateTime.now().month,
              DateTime.now().day,
            ),
          ),
        );

  @override
  Stream<CalendarState> eventHandler(CalendarEvent event, CalendarState currentState) async* {
    try {
      if (event is CalendarEventShowHabits) {
        yield CalendarState.showHabits(choosedDate);
      }
    } catch (e) {}
  }


  Future<Map<String, dynamic>> fetchHabits() async {
    final allDays = await DaysRepository.getAllDays();
    final allHabits = await HabitRepository.listOfHabits();

    return {
      'days': allDays,
      'habits': allHabits,
    };
  }

  List<Habit> getChoosedDayHabits(List<Habit> habits, QuerySnapshot days, DateTime date) {
    DateTime todayTime = DateTime(
      date.year,
      date.month,
      date.day,
    );
    List<Habit> choosedDayHabits = [];

    days.documents.forEach((day) {
      final choosedDay = HelperFunctions.getDateTimeFromTimestamp(day.data['date']);
      if (choosedDay == todayTime) {
        habits.forEach((habit) {
          if (day.data['habits'].containsKey(habit.id)) {
            choosedDayHabits.add(habit);
          }
        });
      }
    });

    return choosedDayHabits;
  }

  List<DateTime> getAllStreakDays(QuerySnapshot days) {
    streakDates = [];
    days.documents.forEach(
      (element) {
        bool isAllDone = false;

        isAllDone = HelperFunctions.getDayStreaks(element);
        if (isAllDone) {
          streakDates.add(
            HelperFunctions.getDateTimeFromTimestamp(element.data['date']),
          );
        }
      },
    );
    return streakDates;
  }

  DocumentSnapshot getChoosedDay(QuerySnapshot days, CalendarState state) {
    DocumentSnapshot choosedDay;
    days.documents.forEach(
      (element) {
        final elementDate = HelperFunctions.getDateTimeFromTimestamp(element.data['date']);
        if (elementDate == state.choosedDate) {
          choosedDay = element;
        }
      },
    );
    return choosedDay;
  }

  int countHabits(List<Habit> choosedDayHabits, DocumentSnapshot choosedDay) {
    int countDoneHabits = 0;
    if (choosedDayHabits.isNotEmpty) {
      choosedDay.data['habits'].forEach(
        (key, element) {
          if (element) {
            countDoneHabits++;
          }
        },
      );
    }
    return countDoneHabits;
  }
}
