import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/habits_bloc/habits_event.dart';
import 'package:habit/bloc/habits_bloc/habits_state.dart';
import 'package:habit/repositories/habit_repository.dart';

class HabitsBloc extends BlocEventStateBase<HabitsEvent, HabitsState> {
  @override
  Stream<HabitsState> eventHandler(
      HabitsEvent event, HabitsState currentState) async* {}

  Future<QuerySnapshot> deleteHabit(String habitId) async {
    return await HabitRepository.deleteHabit(habitId);
  }
}
