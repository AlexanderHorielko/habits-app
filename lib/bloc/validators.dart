import 'dart:async';

class Validators {
  static final RegExp _emailCheck =
      RegExp(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");

  static String _email;
  static String _pass;

  final emailTransformer = StreamTransformer<String, String>.fromHandlers(
    handleData: (data, sink) {
      if (_emailCheck.hasMatch(data)) {
        _email = data;
        sink.add(data);
      } else {
        sink.addError('Invalid email');
      }
    },
  );
  final passTransformer = StreamTransformer<String, String>.fromHandlers(
    handleData: (data, sink) {
      if (data.length > 5) {
        _pass = data;
        sink.add(data);
      } else {
        sink.addError('Too short password');
      }
    },
  );

  String emailValue() {
    return _email;
  }

  String passValue() {
    return _pass;
  }
}
