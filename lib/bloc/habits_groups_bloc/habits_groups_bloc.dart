import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/models/habit.dart';
import 'habits_groups_event.dart';
import 'habits_groups_state.dart';

class HabitsGroupsBloc
    extends BlocEventStateBase<HabitsGroupsEvent, HabitsGroupsState> {
  String name;
  List<dynamic> color;

  List<Map<String, dynamic>> getHabitsGroupsData(
      AsyncSnapshot<QuerySnapshot> snapshotGroups, List<Habit> snapshotHabits) {
    List<Map<String, dynamic>> habitsGroups = [];

    snapshotGroups.data.documents.asMap().forEach((index, element) {
      habitsGroups.add({});
      habitsGroups[index] = {
        'id': element.documentID,
        'name': element.data['name'],
        'color': element.data['color'],
        'habits': [],
      };
    });

    snapshotHabits.forEach((habit) {
      habitsGroups.asMap().forEach((index, group) {
        if (habit.habitGroup == group['name']) {
          habitsGroups[index]['habits'].add(habit);
        }
      });
    });

    return habitsGroups;
  }

  @override
  Stream<HabitsGroupsState> eventHandler(
      HabitsGroupsEvent event, HabitsGroupsState currentState) async* {}
}
