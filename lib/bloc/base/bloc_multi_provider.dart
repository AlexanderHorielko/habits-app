import 'package:flutter/material.dart';

import 'bloc_nested.dart';
import 'bloc_provider.dart';

class MultiBlocProvider extends StatefulWidget {
  final List<BlocProvider> blocBuilders;
  final Widget child;
  MultiBlocProvider({
    Key key,
    @required this.blocBuilders,
    @required this.child,
  }) : super(key: key);

  @override
  _MultiBlocProviderState createState() => _MultiBlocProviderState();
}

class _MultiBlocProviderState extends State<MultiBlocProvider> {
  @override
  Widget build(BuildContext context) {
    var blocProvider = buildBlocProvider(widget.blocBuilders.length, 0);
    return blocProvider;
  }

  Widget buildBlocProvider(int length, int index) {
    // widget.blocBuilders[index].child = buildBlocProvider(length, index + 1);
    if (index != length - 1) {
      return BlocProvider(
        child: buildBlocProvider(length, index + 1),
        blocBuilder: widget.blocBuilders[index].blocBuilder,
      );
    }

    return BlocProvider(child: widget.child, blocBuilder: widget.blocBuilders[index].blocBuilder);
  }
}

// class MultiBlocProvider extends MultiProvider {
//   /// {@macro multi_bloc_provider}
//   MultiBlocProvider({
//     Key key,
//     @required List<BlocProviderSingleChildWidget> providers,
//     @required Widget child,
//   })  : assert(providers != null),
//         assert(child != null),
//         super(key: key, providers: providers, child: child);
// }

// class MultiProvider extends Nested {
//   /// Build a tree of providers from a list of [SingleChildWidget].
//   ///
//   /// The parameter `builder` is syntactic sugar for obtaining a [BuildContext] that can
//   /// read the providers created.
//   ///
//   /// This code:
//   ///
//   /// ```dart
//   /// MultiProvider(
//   ///   providers: [
//   ///     Provider<Something>(create: (_) => Something()),
//   ///     Provider<SomethingElse>(create: (_) => SomethingElse()),
//   ///     Provider<AnotherThing>(create: (_) => AnotherThing()),
//   ///   ],
//   ///   builder: (context, child) {
//   ///     final something = context.watch<Something>();
//   ///     return Text('$something');
//   ///   },
//   /// )
//   /// ```
//   ///
//   /// is strictly equivalent to:
//   ///
//   /// ```dart
//   /// MultiProvider(
//   ///   providers: [
//   ///     Provider<Something>(create: (_) => Something()),
//   ///     Provider<SomethingElse>(create: (_) => SomethingElse()),
//   ///     Provider<AnotherThing>(create: (_) => AnotherThing()),
//   ///   ],
//   ///   child: Builder(
//   ///     builder: (context) {
//   ///       final something = context.watch<Something>();
//   ///       return Text('$something');
//   ///     },
//   ///   ),
//   /// )
//   /// ```
//   ///
//   /// For an explanation on the `child` parameter that `builder` receives,
//   /// see the "Performance optimizations" section of [AnimatedBuilder].
//   MultiProvider({
//     Key key,
//     @required List<SingleChildWidget> providers,
//     Widget child,
//     TransitionBuilder builder,
//   })  : assert(providers != null),
//         super(
//           key: key,
//           children: providers,
//           child: builder != null
//               ? Builder(
//                   builder: (context) => builder(context, child),
//                 )
//               : child,
//         );
// }

// mixin BlocProviderSingleChildWidget on SingleChildWidget {}
