import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';

class AddGroupState extends BlocState {
  final String error;
  final String name;
  final List<dynamic> color;

  AddGroupState({@required this.name, @required this.color, this.error});

  factory AddGroupState.initialState() {
    return AddGroupState(color: [0, 0, 0, 1], name: '');
  }

  factory AddGroupState.error(
      String message, List<dynamic> color, String name) {
    return AddGroupState(error: message, color: color, name: name);
  }

  factory AddGroupState.nameChange(String name, List<dynamic> color) {
    return AddGroupState(name: name, color: color);
  }

  factory AddGroupState.colorChange(List<dynamic> color, String name) {
    return AddGroupState(color: color, name: name);
  }
}
