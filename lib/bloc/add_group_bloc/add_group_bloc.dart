import 'package:flutter/material.dart';
import 'package:habit/bloc/add_group_bloc/add_group_event.dart';
import 'package:habit/bloc/add_group_bloc/add_group_state.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit_group.dart';
import 'package:habit/repositories/habits_group_repository.dart';

class AddGroupBloc extends BlocEventStateBase<AddGroupEvent, AddGroupState> {
  List<dynamic> color = [0, 0, 0, 1];
  String name = '';

  AddGroupBloc() : super(initialState: AddGroupState.initialState());
  @override
  Stream<AddGroupState> eventHandler(AddGroupEvent event, AddGroupState currentState) async* {
    try {
      if (event is AddGroupEventChangeName) {
        name = event.name;
        yield AddGroupState.nameChange(name, color);
      } else if (event is AddGroupEventChangeColor) {
        color = event.color;
        yield AddGroupState.colorChange(color, name);
      }
    } catch (e) {
      yield AddGroupState.error(e.message, color, name);
    }
  }

  Future<void> addGroup() async {
    final newGroup = HabitGroup(name, color);
    await HabitsGroupRepository.addGroup(newGroup);
  }

  Future<void> updateGroup(String oldName) async {
    final newGroup = HabitGroup(name, color);
    await HabitsGroupRepository.updateGroup(newGroup, oldName);
  }

  Future<void> sendGroup(String title, String oldName, BuildContext context) async {
    try {
      title != null ? await updateGroup(oldName) : await addGroup();
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(context, 'Successfully added', color: Theme.of(context).highlightColor),
      );
      await Future.delayed(const Duration(seconds: 1));
      Navigator.of(context).pop();
    } catch (e) {
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(
          context,
          e.message,
          isError: true,
        ),
      );
    }
  }
}
