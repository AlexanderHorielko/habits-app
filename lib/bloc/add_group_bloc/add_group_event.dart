import 'package:habit/bloc/base/bloc_event_state_base.dart';

abstract class AddGroupEvent extends BlocEvent {}

class AddGroupEventChangeName extends AddGroupEvent {
  final String name;

  AddGroupEventChangeName(this.name);
}

class AddGroupEventChangeColor extends AddGroupEvent {
  final List<dynamic> color;

  AddGroupEventChangeColor(this.color);
}
