import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/settings_bloc/settings_event.dart';
import 'package:habit/bloc/settings_bloc/settings_state.dart';
import 'package:habit/repositories/settings_repository.dart';

class SettingsBloc extends BlocEventStateBase<SettingsEvent, SettingsState> {
  bool notificationsOn = false;
  bool inspirationTextOn = false;
  String language = 'English';
  String theme = 'White theme';
  double screenSizeRelation = 0.0;
  bool settingsWereChanged = false;

  // SettingsBloc()
  //     : super(initialState: SettingsState.initialState());

  @override
  Stream<SettingsState> eventHandler(SettingsEvent event, SettingsState currentState) async* {
    try {
      if (event is SettingsEventSwitchNotifications) {
        notificationsOn = !notificationsOn;
        try {
          await SettingsRepository.sendNotificationSwitch(notificationsOn);
        } catch (e) {
          notificationsOn = !notificationsOn;
        }

        yield SettingsState.notificationsOn(
          notificationsOn,
          inspirationTextOn,
          language,
          theme,
        );
      } else if (event is SettingsEventSwitchInspirationText) {
        inspirationTextOn = !inspirationTextOn;

        try {
          await SettingsRepository.sendInspirationTextSwitch(inspirationTextOn);
        } catch (e) {
          inspirationTextOn = !inspirationTextOn;
        }

        yield SettingsState.inspirationTextOn(
          inspirationTextOn,
          notificationsOn,
          language,
          theme,
        );
      } else if (event is SettingsEventSwitchLanguage) {
        await SettingsRepository.sendLanguage(event.lang);

        yield SettingsState.languageSwitch(
          event.lang,
          inspirationTextOn,
          notificationsOn,
          theme,
        );
      } else if (event is SettingsEventSwitchTheme) {
        await SettingsRepository.sendTheme(event.theme);
        yield SettingsState.themeSwitch(
          event.theme,
          language,
          inspirationTextOn,
          notificationsOn,
        );
      } else if (event is SettingsEventSetSettings) {
        yield SettingsState.setSettings(event.settings);
        setSettingsData(event.settings);
      }
    } catch (e) {
      yield SettingsState.error(e.message);
    }
  }

  Future<Map<String, dynamic>> getSettings() async {
    return await SettingsRepository.getSettingsData();
  }

  void setSettingsData(Map<String, dynamic> settingsData) {
    notificationsOn = settingsData['notification'];
    inspirationTextOn = settingsData['inspirationText'];
    language = settingsData['language'];
    theme = settingsData['theme'];
  }
}
