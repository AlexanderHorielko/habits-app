import '../base/bloc_event_state_base.dart';

abstract class SettingsEvent extends BlocEvent {}

class SettingsEventSwitchNotifications extends SettingsEvent {}

class SettingsEventSwitchInspirationText extends SettingsEvent {}

class SettingsEventSwitchLanguage extends SettingsEvent {
  final String lang;
  SettingsEventSwitchLanguage(this.lang);
}

class SettingsEventSwitchTheme extends SettingsEvent {
  final String theme;
  SettingsEventSwitchTheme(this.theme);
}

class SettingsEventSetSettings extends SettingsEvent {
  final Map<String, dynamic> settings;

  SettingsEventSetSettings(this.settings);
}
