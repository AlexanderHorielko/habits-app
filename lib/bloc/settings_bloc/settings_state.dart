import 'package:habit/bloc/base/bloc_event_state_base.dart';

class SettingsState extends BlocState {
  final bool notificationsOn;
  final bool inspirationTextOn;
  final String error;
  final String language;
  final String themeMode;

  SettingsState({
    this.notificationsOn,
    this.inspirationTextOn,
    this.error,
    this.language,
    this.themeMode,
  });

  factory SettingsState.initialState() {
    return SettingsState(
      notificationsOn: true,
      inspirationTextOn: true,
      language: 'English',
      themeMode: 'White theme',
      error: '',
    );
  }

  factory SettingsState.notificationsOn(
    bool notificationsOn,
    bool inspirationTextOn,
    String lang,
    String theme,
  ) {
    return SettingsState(
      themeMode: theme,
      language: lang,
      notificationsOn: notificationsOn,
      inspirationTextOn: inspirationTextOn,
    );
  }

  factory SettingsState.inspirationTextOn(
    bool inspirationTextOn,
    bool notificationsOn,
    String lang,
    String theme,
  ) {
    return SettingsState(
      themeMode: theme,
      language: lang,
      inspirationTextOn: inspirationTextOn,
      notificationsOn: notificationsOn,
    );
  }

  factory SettingsState.error(String value) {
    return SettingsState(error: value);
  }

  factory SettingsState.languageSwitch(
    String lang,
    bool inspirationTextOn,
    bool notificationsOn,
    String theme,
  ) {
    return SettingsState(
      themeMode: theme,
      language: lang,
      inspirationTextOn: inspirationTextOn,
      notificationsOn: notificationsOn,
    );
  }

  factory SettingsState.themeSwitch(
    String theme,
    String lang,
    bool inspirationTextOn,
    bool notificationsOn,
  ) {
    return SettingsState(
      themeMode: theme,
      language: lang,
      inspirationTextOn: inspirationTextOn,
      notificationsOn: notificationsOn,
    );
  }

  factory SettingsState.setSettings(Map<String, dynamic> settingsData) {
    return SettingsState(
      themeMode: settingsData['theme'],
      language: settingsData['language'],
      inspirationTextOn: settingsData['inspirationText'],
      notificationsOn: settingsData['notification'],
    );
  }
}
