import '../base/bloc_event_state_base.dart';

abstract class AuthEvent extends BlocEvent {}

class AuthEventLogin extends AuthEvent {}

class AuthEventLogout extends AuthEvent {}

class AuthEventSignUp extends AuthEvent {}

class AuthEventSignUpWithGoogle extends AuthEvent {}

class AuthEventChangeScreen extends AuthEvent {}

class AuthEventAppStarted extends AuthEvent {}
