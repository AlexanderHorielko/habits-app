import 'package:flutter/foundation.dart';

import '../base/bloc_event_state_base.dart';

class AuthState extends BlocState {
  AuthState({
    @required this.isAuthenticated,
    this.isAuthenticating: false,
    this.hasFailed: false,
    this.error: '',
    this.changeScreen: false,
  });

  final bool isAuthenticated;
  final bool isAuthenticating;
  final bool hasFailed;
  final bool changeScreen;

  final String error;

  factory AuthState.notAuthenticated() {
    return AuthState(
      isAuthenticated: false,
    );
  }

  factory AuthState.authenticated() {
    return AuthState(
      isAuthenticated: true,
    );
  }

  factory AuthState.authenticating() {
    return AuthState(
      isAuthenticated: false,
      isAuthenticating: true,
    );
  }

  factory AuthState.failure(String err) {
    return AuthState(
      isAuthenticated: false,
      hasFailed: true,
      error: err,
    );
  }

  factory AuthState.changeScreen(bool value) {
    return AuthState(
      isAuthenticated: false,
      changeScreen: value,
    );
  }
}
