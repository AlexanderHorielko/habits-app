import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:habit/bloc/auth_bloc/auth_event.dart';
import 'package:habit/bloc/auth_bloc/auth_state.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/validators.dart';
import 'package:habit/repositories/firebase_auth_repository.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc extends BlocEventStateBase<AuthEvent, AuthState> with Validators {
  FirebaseUser user;
  bool _changeScreen = true;

  BehaviorSubject<String> _emailController = BehaviorSubject<String>();
  BehaviorSubject<String> _passController = BehaviorSubject<String>();

  AuthBloc()
      : super(
          initialState: AuthState.notAuthenticated(),
        );

  @override
  Stream<AuthState> eventHandler(AuthEvent event, AuthState currentState) async* {
    try {
      if (event is AuthEventLogin) {
        yield AuthState.authenticating();

        final res = await FirebaseAuthRepository.signIn(emailValue(), passValue());
        _emailController.value = '';
        _passController.value = '';
        yield AuthState.authenticated();
        user = res.user;
      } else if (event is AuthEventSignUp) {
        yield AuthState.authenticating();

        final res = await FirebaseAuthRepository.createUser(email: emailValue(), password: passValue());

        _emailController.value = '';
        _passController.value = '';
        yield AuthState.authenticated();
        user = res.user;
      } else if (event is AuthEventSignUpWithGoogle) {
        yield AuthState.authenticating();

        final res = await FirebaseAuthRepository.createUser(signUpWithGoogle: true);
        yield AuthState.authenticated();
        user = res.user;
      } else if (event is AuthEventChangeScreen) {
        _changeScreen = !_changeScreen;
        yield AuthState.changeScreen(!_changeScreen);
      } else if (event is AuthEventLogout) {
        yield AuthState.authenticating();
        await FirebaseAuthRepository.signOut();
        yield AuthState.notAuthenticated();
      } else if (event is AuthEventAppStarted) {
        final userRes = await FirebaseAuthRepository.getCurrentUser();
        if (userRes != null) {
          yield AuthState.authenticated();
          user = userRes;
        } else {
          yield AuthState.notAuthenticated();
        }
      }
    } catch (error) {
      _emailController.value = '';
      _passController.value = '';
      yield AuthState.failure(error.message);
    }
  }

  Function(String) get changeEmail => _emailController.sink.add;
  Function(String) get changePassword => _passController.sink.add;

  Stream<String> get email => _emailController.stream.transform(emailTransformer);
  Stream<String> get password => _passController.stream.transform(passTransformer);

  Stream<bool> get submit => CombineLatestStream.combine2(email, password, (a, b) => true);

  void dispose() {
    _emailController.close();
    _passController.close();
  }
}
