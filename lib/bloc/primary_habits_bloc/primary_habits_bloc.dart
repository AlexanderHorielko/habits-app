import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/primary_habits_bloc/primary_habits_event.dart';
import 'package:habit/bloc/primary_habits_bloc/primary_habits_state.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/habits_group_repository.dart';
import 'package:habit/repositories/primary_habits_repository.dart';
import 'package:intl/intl.dart';

class PrimaryHabitsBloc
    extends BlocEventStateBase<PrimaryHabitsEvent, PrimaryHabitsState> {
  @override
  Stream<PrimaryHabitsState> eventHandler(
      PrimaryHabitsEvent event, PrimaryHabitsState currentState) async* {}

  Map<String, List<List<DateTime>>> getAllStreaks(
      List<Habit> habits, QuerySnapshot days) {
    Map<String, List<List<DateTime>>> streaks = {};
    habits.forEach((element) {
      streaks[element.id] = (HelperFunctions.getHabitStreak(element, days));
    });
    return streaks;
  }

  int getCurrentStreak(Habit currentHabit, QuerySnapshot days) {
    final streaks = HelperFunctions.getHabitStreak(currentHabit, days);
    if (streaks.isEmpty) {
      return 0;
    }

    return streaks.last.length;
  }

  int getBestStreak(Habit currentHabit, QuerySnapshot days) {
    final streaks = HelperFunctions.getHabitStreak(currentHabit, days);
    int maxLength = -1;
    if (streaks.isEmpty) {
      return 0;
    }

    streaks.forEach((element) {
      if (element.length > maxLength) {
        maxLength = element.length;
      }
    });
    return maxLength;
  }

  String getLastBreak(Habit currentHabit, QuerySnapshot days) {
    final streaks = HelperFunctions.getHabitStreak(currentHabit, days);
    if (streaks.length < 2) {
      return '-';
    }

    return DateFormat('dd.MM.yy').format(streaks[streaks.length - 2].last);
  }

  Future<Map<String, dynamic>> allPrimaryHabitsData() async {
    final primaryHabits = await PrimaryHabitsRepository.getPrimaryHabits();
    final groups = await HabitsGroupRepository.getAllGroups();
    final days = await DaysRepository.getAllDays();
    // final streaks = getAllStreaks(primaryHabits, days);

    return {
      'primaryHabits': primaryHabits,
      'groups': groups,
      'days': days,
    };
  }
}
