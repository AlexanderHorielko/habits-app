import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_state.dart';

class BottomNavbarBloc extends BlocEventStateBase<BottomNavbarEvent, BottomNavbarState> {
  String _currentTab = '';

  BottomNavbarBloc() : super(initialState: BottomNavbarState.initialState());

  @override
  Stream<BottomNavbarState> eventHandler(BottomNavbarEvent event, BottomNavbarState currentState) async* {
    try {
      if (event is BottomNavbarEventChangeTab) {
        _currentTab = event.tabName;
        yield BottomNavbarState.changeTab(_currentTab);
      }
    } catch (e) {}
  }
}
