import 'package:habit/bloc/base/bloc_event_state_base.dart';

class BottomNavbarState extends BlocState {
  final String currentTab;

  BottomNavbarState({this.currentTab});

  factory BottomNavbarState.initialState() {
    return BottomNavbarState(currentTab: 'Daily');
  }

  factory BottomNavbarState.changeTab(String tab) {
    return BottomNavbarState(currentTab: tab);
  }
}
