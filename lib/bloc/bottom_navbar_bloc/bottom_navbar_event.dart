import 'package:habit/bloc/base/bloc_event_state_base.dart';

abstract class BottomNavbarEvent extends BlocEvent {}

class BottomNavbarEventChangeTab extends BottomNavbarEvent {
  final String tabName;

  BottomNavbarEventChangeTab(this.tabName);
}
