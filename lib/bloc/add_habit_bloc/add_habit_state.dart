import 'package:habit/bloc/base/bloc_event_state_base.dart';

class AddHabitState extends BlocState {
  final String name;
  final String habitsGroup;
  final String habitName;
  final String frequency;
  // final bool showPeriodization;
  final DateTime remindTime;
  final String error;
  final bool isPrimary;
  final bool remindHabits;

  AddHabitState({
    this.name,
    this.habitsGroup,
    this.habitName,
    this.frequency,
    this.remindTime,
    this.error,
    // this.showPeriodization = false,
    this.isPrimary = false,
    this.remindHabits = false,
  });

  factory AddHabitState.initialState() {
    return AddHabitState(
      habitsGroup: '',
      frequency: 'Every day',
    );
  }

  factory AddHabitState.error(String message) {
    return AddHabitState(error: message);
  }

  factory AddHabitState.changeGroup(
    String group,
    String periodization,
    bool switchPrimary,
    bool switchHabitsRemind,
    String name,
    DateTime time,
  ) {
    return AddHabitState(
      habitsGroup: group,
      frequency: periodization,
      isPrimary: switchPrimary,
      remindHabits: switchHabitsRemind,
      name: name,
      remindTime: time,
    );
  }

  factory AddHabitState.choosePeriodization(
    String periodization,
    String group,
    bool switchPrimary,
    bool switchHabitsRemind,
    String name,
    DateTime time,
  ) {
    return AddHabitState(
      frequency: periodization,
      habitsGroup: group,
      isPrimary: switchPrimary,
      remindHabits: switchHabitsRemind,
      name: name,
      remindTime: time,
    );
  }

  // factory AddHabitState.showPeriodization(
  //   bool showPeriodization,
  //   String periodization,
  //   String group,
  //   bool switchPrimary,
  //   bool switchHabitsRemind,
  //   String name,
  //   DateTime time,
  // ) {
  //   return AddHabitState(
  //     // showPeriodization: showPeriodization,
  //     frequency: periodization,
  //     habitsGroup: group,
  //     isPrimary: switchPrimary,
  //     remindHabits: switchHabitsRemind,
  //     name: name,
  //     remindTime: time,
  //   );
  // }

  factory AddHabitState.switchPrimary(
    bool switchPrimary,
    bool switchHabitsRemind,
    String periodization,
    String group,
    String name,
    DateTime time,
  ) {
    return AddHabitState(
      isPrimary: switchPrimary,
      remindHabits: switchHabitsRemind,
      habitsGroup: group,
      frequency: periodization,
      name: name,
      remindTime: time,
    );
  }

  factory AddHabitState.switchHabitsRemind(
    bool switchHabitsRemind,
    bool switchPrimary,
    String periodization,
    String group,
    String name,
    DateTime time,
  ) {
    return AddHabitState(
      remindHabits: switchHabitsRemind,
      isPrimary: switchPrimary,
      habitsGroup: group,
      frequency: periodization,
      name: name,
      remindTime: time,
    );
  }

  factory AddHabitState.inputChange(
    String name,
    String periodization,
    String group,
    bool switchHabitsRemind,
    bool switchPrimary,
    DateTime time,
  ) {
    return AddHabitState(
      name: name,
      remindHabits: switchHabitsRemind,
      isPrimary: switchPrimary,
      habitsGroup: group,
      frequency: periodization,
      remindTime: time,
    );
  }

  factory AddHabitState.remindTimePicked(
    DateTime time,
    String name,
    String periodization,
    String group,
    bool switchHabitsRemind,
    bool switchPrimary,
  ) {
    return AddHabitState(
      remindTime: time,
      name: name,
      remindHabits: switchHabitsRemind,
      isPrimary: switchPrimary,
      habitsGroup: group,
      frequency: periodization,
    );
  }
}
