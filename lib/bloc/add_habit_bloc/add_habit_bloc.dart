import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_event_state_base.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/habit_repository.dart';

class AddHabitBloc extends BlocEventStateBase<AddHabitEvent, AddHabitState> {
  Habit habit = Habit(
    name: null,
    habitGroup: '',
    frequency: 'Every day',
    isPrimary: false,
    remindHabits: false,
    isDone: false,
    remindTime: DateTime.now(),
    created: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
  );

  List<String> hints = [];
  TextEditingController nameInputController = TextEditingController();
  double opacity = 1;
  bool isAddingPrimaryHabit = false;

  AddHabitBloc() : super(initialState: AddHabitState.initialState());

  @override
  Stream<AddHabitState> eventHandler(AddHabitEvent event, AddHabitState currentState) async* {
    try {
      if (event is AddHabitEventChangeGroup) {
        habit.habitGroup = event.groupName;
        yield AddHabitState.changeGroup(
          habit.habitGroup,
          habit.frequency,
          habit.isPrimary,
          habit.remindHabits,
          habit.name,
          habit.remindTime,
        );
      } else if (event is AddHabitEventShowPeriodization) {
      } else if (event is AddHabitEventChoosePeriodization) {
        habit.frequency = event.periodization;

        yield AddHabitState.choosePeriodization(
          habit.frequency,
          habit.habitGroup,
          habit.isPrimary,
          habit.remindHabits,
          habit.name,
          habit.remindTime,
        );
      } else if (event is AddHabitEventSwitchPrimary) {
        habit.isPrimary = !habit.isPrimary;

        yield AddHabitState.switchPrimary(
          habit.isPrimary,
          habit.remindHabits,
          habit.frequency,
          habit.habitGroup,
          habit.name,
          habit.remindTime,
        );
      } else if (event is AddHabitEventSwitchHabitsRemind) {
        habit.remindHabits = !habit.remindHabits;

        yield AddHabitState.switchHabitsRemind(
          habit.remindHabits,
          habit.isPrimary,
          habit.frequency,
          habit.habitGroup,
          habit.name,
          habit.remindTime,
        );
      } else if (event is AddHabitEventInputChange) {
        habit.name = event.habitName;

        yield AddHabitState.inputChange(
          event.habitName,
          habit.frequency,
          habit.habitGroup,
          habit.remindHabits,
          habit.isPrimary,
          habit.remindTime,
        );
      } else if (event is AddHabitEventTimePicked) {
        habit.remindTime = event.time;

        yield AddHabitState.remindTimePicked(
          habit.remindTime,
          habit.name,
          habit.frequency,
          habit.habitGroup,
          habit.remindHabits,
          habit.isPrimary,
        );
      }
    } catch (e) {
      yield AddHabitState.error(e.message);
    }
  }

  Widget buildGroups(BuildContext context, QuerySnapshot snapshot, AddHabitState addHabitState) {
    List<Widget> children = [];
    List<List<Widget>> columns = [[], [], []];
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    Widget addNewButton = HelperFunctions.buildOption(
      context,
      'Add new +',
      false,
      'addNewOption',
    );

    if (snapshot.documents.length == 0) {
      columns[0].add(addNewButton);
    }

    for (var i = 0; i < snapshot.documents.length; i++) {
      final element = snapshot.documents[i];
      Widget groupWidget = Container(
        margin: EdgeInsets.only(bottom: 10 * settingsBloc.screenSizeRelation),
        child: HelperFunctions.buildOption(
          context,
          element.data['name'],
          addHabitState.habitsGroup == element.data['name'],
          'habitsOption',
        ),
      );

      if (i % 3 == 0) {
        columns[0].add(groupWidget);
      } else if (i % 3 == 1) {
        columns[1].add(groupWidget);
      } else if (i % 3 == 2) {
        columns[2].add(groupWidget);
      }

      if (i == snapshot.documents.length - 1) {
        final columnIndex = (i + 1) % 3;
        columns[columnIndex].add(addNewButton);
      }
    }

    columns.forEach(
      (column) {
        children.add(
          Container(
            margin: EdgeInsets.only(
              bottom: 10 * settingsBloc.screenSizeRelation,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ...column,
              ],
            ),
          ),
        );
      },
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...children,
      ],
    );
  }

  List<dynamic> buildHints(BuildContext context, List<dynamic> snap) {
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    final List<dynamic> hints = snap.map(
      (element) {
        return GestureDetector(
          child: Text(
            element,
            style: TextStyle(
              fontSize: 14 * settingsBloc.screenSizeRelation,
              color: Theme.of(context).primaryColorDark.withOpacity(0.7),
            ),
          ),
          onTap: () {
            opacity = 0;
            addHabitBloc.emitEvent(
              AddHabitEventInputChange(element),
            );
            nameInputController.text = habit.name;
          },
        );
      },
    ).toList();
    return hints;
  }

  Future<void> sendHabit(BuildContext context) async {
    try {
      await HabitRepository.addHabit(habit);
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(
          context,
          'Successfully added',
          color: Theme.of(context).highlightColor,
        ),
      );
      await Future.delayed(const Duration(seconds: 1));
      Navigator.of(context).pop(true);
    } catch (e) {
      Scaffold.of(context).showSnackBar(
        HelperFunctions.buildSnackBar(
          context,
          e.message,
          isError: true,
        ),
      );
    }
  }
}
