import 'package:habit/bloc/base/bloc_event_state_base.dart';

abstract class AddHabitEvent extends BlocEvent {}

class AddHabitEventChangeGroup extends AddHabitEvent {
  final String groupName;
  AddHabitEventChangeGroup(this.groupName);
}

class AddHabitEventChoosePeriodization extends AddHabitEvent {
  final String periodization;
  AddHabitEventChoosePeriodization(this.periodization);
}

class AddHabitEventShowPeriodization extends AddHabitEvent {}

class AddHabitEventSwitchPrimary extends AddHabitEvent {}

class AddHabitEventSwitchHabitsRemind extends AddHabitEvent {}

class AddHabitEventInputChange extends AddHabitEvent {
  final String habitName;

  AddHabitEventInputChange(this.habitName);
}

class AddHabitEventTimePicked extends AddHabitEvent {
  final DateTime time;

  AddHabitEventTimePicked(this.time);
}
