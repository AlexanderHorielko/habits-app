import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/repositories/habit_repository.dart';
import 'package:habit/widgets/add_habit_screen/all_groups.dart';
import 'package:habit/widgets/add_habit_screen/name_input.dart';
import 'package:habit/widgets/add_habit_screen/primary_switch.dart';
import 'package:habit/widgets/add_habit_screen/remind_habits_switch.dart';
import 'package:habit/widgets/add_habit_screen/select_periodization.dart';
import 'package:habit/widgets/add_habit_screen/send_button.dart';

class AddHabitScreen extends StatefulWidget {
  static const routeName = '/add-habit';
  @override
  _AddHabitScreenState createState() => _AddHabitScreenState();
}

class _AddHabitScreenState extends State<AddHabitScreen> with TickerProviderStateMixin {
  AddHabitBloc _addHabitBloc = AddHabitBloc();
  TextEditingController textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    final arguments = ModalRoute.of(context).settings.arguments;
    if (arguments != null) {
      if (arguments == 'isPrimary') {
        _addHabitBloc.isAddingPrimaryHabit = true;
        _addHabitBloc.emitEvent(AddHabitEventSwitchPrimary());
      } else {
        _addHabitBloc.isAddingPrimaryHabit = false;
        _addHabitBloc.emitEvent(AddHabitEventChangeGroup(arguments));
      }
    }

    return BlocProvider<AddHabitBloc>(
      blocBuilder: () => _addHabitBloc,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: FutureBuilder(
          future: HabitRepository.getAddHabitData(),
          // future: _addHabitBloc.getAddHabitData(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }

            return Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                      top: 74 * settingsBloc.screenSizeRelation,
                      bottom: 78 * settingsBloc.screenSizeRelation,
                      left: 16 * settingsBloc.screenSizeRelation,
                      right: 16 * settingsBloc.screenSizeRelation,
                    ),
                    child: BlocEventStateBuilder<AddHabitEvent, AddHabitState>(
                      bloc: _addHabitBloc,
                      builder: (context, state) {
                        return Container(
                          height: MediaQuery.of(context).size.height -
                              MediaQuery.of(context).padding.top -
                              178 * settingsBloc.screenSizeRelation,
                          child: ListView.builder(
                            padding: EdgeInsets.only(top: 0),
                            itemCount: 1,
                            itemBuilder: (context, index) {
                              return Column(
                                children: <Widget>[
                                  AllGroups(snapshot.data['groups'], state),
                                  if (state.habitsGroup == '')
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 10 * settingsBloc.screenSizeRelation,
                                      ),
                                      width: double.infinity,
                                      child: Text(
                                        'Choose or add new group',
                                        style: TextStyle(
                                          fontSize: 13 * settingsBloc.screenSizeRelation,
                                          color: Theme.of(context).errorColor,
                                        ),
                                      ),
                                    ),
                                  NameInput(state),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 20 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        ..._addHabitBloc.buildHints(
                                          context,
                                          snapshot.data['hints'].data['hints'],
                                        ),
                                      ],
                                    ),
                                  ),
                                  SelectPeriodization(),
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 65 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        PrimarySwitch(),
                                        RemindHabitsSwitch(state),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ),
                  Spacer(),
                  SendButton(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
