import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_state.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/main.dart';
import 'package:habit/screens/calendar_screen.dart';
import 'package:habit/screens/habits_groups_screen.dart';
import 'package:habit/screens/habits_screen.dart';
import 'package:habit/screens/primary_habits_screen.dart';
import 'package:habit/screens/settings_screen.dart';
import 'package:habit/widgets/home_screen/bottom_navbar.dart';
import 'package:habit/widgets/home_screen/daily_habits.dart';
import 'package:habit/widgets/home_screen/daily_quotes.dart';
import 'package:habit/widgets/home_screen/minimized_calendar.dart';
import 'package:habit/widgets/home_screen/navbar.dart';

import 'add_group_screen.dart';
import 'add_habit_screen.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<HomeScreen> {
  Future<bool> _onWillPopScope() async {
    return false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);
    settingsBloc.screenSizeRelation = MediaQuery.of(context).size.height / MyApp.defaultHeight;
    return WillPopScope(
      onWillPop: _onWillPopScope,
      child: BlocEventStateBuilder<BottomNavbarEvent, BottomNavbarState>(
        bloc: bottomNavbarBloc,
        builder: (context, state) {
          return Scaffold(
            appBar: state.currentTab == 'Daily'
                ? PreferredSize(
                    preferredSize: Size(
                      double.infinity,
                      115 * settingsBloc.screenSizeRelation,
                    ),
                    child: Navbar('Daily'),
                  )
                : PreferredSize(
                    preferredSize: Size(
                      double.infinity,
                      220 * settingsBloc.screenSizeRelation,
                    ),
                    child: state.currentTab == 'Primary'
                        ? Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 16 * settingsBloc.screenSizeRelation,
                                ),
                                child: Navbar(
                                  'Primary',
                                  title: 'Primary habits',
                                ),
                              ),
                              Spacer(),
                              HelperFunctions.buildAddButton(
                                context,
                                'Add primary habits',
                                AddHabitScreen.routeName,
                                arguments: 'isPrimary',
                              ),
                            ],
                          )
                        : state.currentTab == 'HabitsGroup'
                            ? Column(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 16 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Navbar(
                                      'HabitsGroup',
                                      title: 'Habits group',
                                    ),
                                  ),
                                  Spacer(),
                                  HelperFunctions.buildAddButton(
                                    context,
                                    'Add groups',
                                    AddGroupScreen.routeName,
                                  ),
                                ],
                              )
                            : state.currentTab == 'Settings'
                                ? PreferredSize(
                                    preferredSize: Size(
                                      double.infinity,
                                      115 * settingsBloc.screenSizeRelation,
                                    ),
                                    child: Navbar(
                                      'Daily',
                                      title: 'Settings',
                                      iconButton: IconButton(
                                        icon: Icon(Icons.close),
                                        iconSize: 30,
                                        color: Theme.of(context).primaryColorDark,
                                        onPressed: () {
                                          bottomNavbarBloc.emitEvent(
                                            BottomNavbarEventChangeTab(homeBloc.previousTabName),
                                          );
                                        },
                                      ),
                                    ),
                                  )
                                : state.currentTab == 'Habits'
                                    ? PreferredSize(
                                        preferredSize: Size(
                                          double.infinity,
                                          220 * settingsBloc.screenSizeRelation,
                                        ),
                                        child: GestureDetector(
                                          child: Container(
                                            color: HelperFunctions.buildColor(homeBloc.arguments['color']),
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: 16 * settingsBloc.screenSizeRelation,
                                                  ),
                                                  child: Navbar(
                                                    'Habits',
                                                    title: homeBloc.arguments['name'],
                                                    textColor: HelperFunctions.buildColor(homeBloc.arguments['color'])
                                                                .computeLuminance() >
                                                            0.5
                                                        ? Colors.black
                                                        : Colors.white,
                                                  ),
                                                ),
                                                Spacer(),
                                                HelperFunctions.buildAddButton(
                                                  context,
                                                  'Add habits',
                                                  AddHabitScreen.routeName,
                                                  arguments: homeBloc.arguments['name'],
                                                ),
                                              ],
                                            ),
                                          ),
                                          onTap: () {
                                            homeBloc.arguments = {
                                              'name': homeBloc.arguments['name'],
                                              'color': homeBloc.arguments['color'],
                                            };
                                            bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('AddGroup'));
                                          },
                                        ),
                                      )
                                    : state.currentTab == 'Calendar'
                                        ? PreferredSize(
                                            preferredSize: Size(
                                              double.infinity,
                                              115 * settingsBloc.screenSizeRelation,
                                            ),
                                            child: Navbar('Calendar'),
                                          )
                                        : state.currentTab == 'Progress' ? Container() : Container(),
                  ),
            body: Column(
              children: [
                state.currentTab == 'Daily'
                    ? Container(
                        child: Column(
                          children: <Widget>[
                            MinimizedCalendar(),
                            DailyQuotes(),
                            DailyHabits(),
                          ],
                        ),
                      )
                    : state.currentTab == 'Primary'
                        ? PrimaryHabitsScreen()
                        : state.currentTab == 'HabitsGroup'
                            ? HabitsGroupsScreen()
                            : state.currentTab == 'Settings'
                                ? SettingsScreen()
                                : state.currentTab == 'Habits'
                                    ? HabitsScreen()
                                    : state.currentTab == 'Calendar'
                                        ? CalendarScreen()
                                        : state.currentTab == 'Progress' ? Container() : Container(),
                if (state.currentTab != 'Calendar') Spacer(),
                if (state.currentTab != 'Calendar') BottomNavbar(),
              ],
            ),
          );
        },
      ),
    );
  }
}

class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(builder: builder, maintainState: maintainState, settings: settings, fullscreenDialog: fullscreenDialog);

  @override
  Widget buildTransitions(
      BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}
