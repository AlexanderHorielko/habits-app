import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/calendar_bloc/calendar_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/calendar_screen/expanded_calendar.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarScreen extends StatefulWidget {
  static const routeName = '/calendar';

  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  CalendarBloc calendarBloc = CalendarBloc();
  CalendarController _calendarController;
  DateTime previousChoosedDate = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
  );
  double startDragPosition = 0;
  double offset = 0;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);

    return BlocProvider<CalendarBloc>(
      blocBuilder: () => calendarBloc,
      child: FutureBuilder(
        future: calendarBloc.fetchHabits(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Container();
          }
          return Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  ExpandedCalendar(
                    _calendarController,
                    snapshot.data['days'],
                    snapshot.data['habits'],
                  ),
                  GestureDetector(
                    onVerticalDragStart: (details) {
                      startDragPosition = details.globalPosition.dy;
                    },
                    onVerticalDragUpdate: (details) {
                      offset = details.globalPosition.dy - startDragPosition;
                    },
                    onVerticalDragEnd: (details) {
                      if (offset < -100) {
                        bottomNavbarBloc.emitEvent(BottomNavbarEventChangeTab('Daily'));
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 10 * settingsBloc.screenSizeRelation,
                        bottom: 20 * settingsBloc.screenSizeRelation,
                      ),
                      width: 25 * settingsBloc.screenSizeRelation,
                      height: 3 * settingsBloc.screenSizeRelation,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Theme.of(context).primaryColorDark.withOpacity(0.1),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}
