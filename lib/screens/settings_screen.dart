import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/auth_bloc/auth_event.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_event.dart';
import 'package:habit/bloc/settings_bloc/settings_state.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/widgets/settings_screen/custom_switcher.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    settingsBloc.settingsWereChanged = true;

    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 16 * settingsBloc.screenSizeRelation,
              right: 16 * settingsBloc.screenSizeRelation,
              top: 36 * settingsBloc.screenSizeRelation,
            ),
            child: FutureBuilder(
              future: Future.delayed(const Duration(milliseconds: 500)),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.25),
                      child: SizedBox(
                        height: 60,
                        width: 60,
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  );
                }
                return BlocEventStateBuilder<SettingsEvent, SettingsState>(
                  bloc: settingsBloc,
                  builder: (context, state) {
                    if (state == null) {
                      return Container(
                        child: CircularProgressIndicator(),
                      );
                    }
                    if (state.error != null) {
                      Scaffold.of(context).showSnackBar(
                        HelperFunctions.buildSnackBar(context, state.error),
                      );
                    }
                    return Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            children: <Widget>[
                              Text(
                                authBloc.user.email,
                                style: TextStyle(
                                  fontSize: 16 * settingsBloc.screenSizeRelation,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Spacer(),
                              InkWell(
                                child: Text(
                                  'Log out',
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontSize: 16 * settingsBloc.screenSizeRelation,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                onTap: () {
                                  authBloc.emitEvent(AuthEventLogout());
                                },
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                top: 83 * settingsBloc.screenSizeRelation,
                              ),
                              child: _buildHeader('Notification'),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    'Turn on/off application notification',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                      fontSize: 14 * settingsBloc.screenSizeRelation,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  Spacer(),
                                  CustomSwitch(
                                    'Notification',
                                    state,
                                    settingsBloc,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                top: 41 * settingsBloc.screenSizeRelation,
                                bottom: 16 * settingsBloc.screenSizeRelation,
                              ),
                              child: _buildHeader('InspirationText'),
                            ),
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    'Turn on/off inspiration text',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                      fontSize: 14 * settingsBloc.screenSizeRelation,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  Spacer(),
                                  CustomSwitch(
                                    'InspirationText',
                                    state,
                                    settingsBloc,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                top: 41 * settingsBloc.screenSizeRelation,
                                bottom: 20 * settingsBloc.screenSizeRelation,
                              ),
                              child: _buildHeader('Language'),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                HelperFunctions.buildOption(
                                    context, 'English', state.language == 'English', 'languageOption'),
                                HelperFunctions.buildOption(
                                    context, 'Spain', state.language == 'Spain', 'languageOption'),
                                HelperFunctions.buildOption(
                                    context, 'Russian', state.language == 'Russian', 'languageOption'),
                              ],
                            )
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                top: 35 * settingsBloc.screenSizeRelation,
                                bottom: 20 * settingsBloc.screenSizeRelation,
                              ),
                              child: _buildHeader('Theme mode'),
                            ),
                            state.themeMode == 'White theme'
                                ? Row(
                                    children: <Widget>[
                                      _buildThemeModeOption('White theme', true, settingsBloc),
                                      _buildThemeModeOption('Dark theme', false, settingsBloc),
                                    ],
                                  )
                                : Row(
                                    children: <Widget>[
                                      _buildThemeModeOption('White theme', false, settingsBloc),
                                      _buildThemeModeOption('Dark theme', true, settingsBloc),
                                    ],
                                  ),
                          ],
                        ),
                      ],
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildHeader(String text) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Container(
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        style: TextStyle(
          fontSize: 20 * settingsBloc.screenSizeRelation,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  Widget _buildThemeModeOption(String name, bool choosed, SettingsBloc settingsBloc) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);

    return Expanded(
      child: Container(
        height: 40 * settingsBloc.screenSizeRelation,
        decoration: BoxDecoration(
          color: choosed ? Theme.of(context).accentColor : Theme.of(context).primaryColorDark.withOpacity(0.03),
          borderRadius: name == 'White theme'
              ? BorderRadius.only(
                  topLeft: Radius.circular(8),
                  bottomLeft: Radius.circular(8),
                )
              : BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                ),
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            child: Center(
              child: Text(
                name,
                style: TextStyle(
                  fontSize: 14 * settingsBloc.screenSizeRelation,
                  fontWeight: FontWeight.w400,
                  color: choosed ? Theme.of(context).primaryColorLight : Theme.of(context).primaryColorDark,
                ),
              ),
            ),
            onTap: () {
              settingsBloc.theme = name;
              settingsBloc.emitEvent(SettingsEventSwitchTheme(name));
            },
          ),
        ),
      ),
    );
  }
}
