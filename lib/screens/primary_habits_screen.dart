import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/primary_habits_bloc/primary_habits_bloc.dart';
import 'package:habit/widgets/primary_habits_screen/primary_habits_list.dart';

class PrimaryHabitsScreen extends StatefulWidget {
  static const routeName = '/primary-habits';
  @override
  _PrimaryHabitsScreenState createState() => _PrimaryHabitsScreenState();
}

class _PrimaryHabitsScreenState extends State<PrimaryHabitsScreen> {
  PrimaryHabitsBloc primaryHabitsBloc = PrimaryHabitsBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocBuilder: () => primaryHabitsBloc,
      child: PrimaryHabitsList(),
    );
  }
}
