import 'package:flutter/material.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/habits_groups_bloc/habits_groups_bloc.dart';
import 'package:habit/widgets/habits_groups_screen/habits_groups_list.dart';

class HabitsGroupsScreen extends StatefulWidget {
  static const routeName = '/habits-groups';

  @override
  _HabitsGroupsScreenState createState() => _HabitsGroupsScreenState();
}

class _HabitsGroupsScreenState extends State<HabitsGroupsScreen> {
  HabitsGroupsBloc _habitsGroupsBloc = HabitsGroupsBloc();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocBuilder: () => _habitsGroupsBloc,
      child: HabitsGroupsList(),
    );
  }
}
