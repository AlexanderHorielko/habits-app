import 'package:flutter/material.dart';
import 'package:habit/bloc/add_group_bloc/add_group_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/add_group_screen/group_color_picker.dart';
import 'package:habit/widgets/add_group_screen/group_name_input.dart';
import 'package:habit/widgets/add_group_screen/send_group_button.dart';
import 'package:habit/widgets/home_screen/navbar.dart';

class AddGroupScreen extends StatefulWidget {
  static const routeName = '/add-group';
  @override
  _AddGroupScreenState createState() => _AddGroupScreenState();
}

class _AddGroupScreenState extends State<AddGroupScreen> {
  AddGroupBloc _addGroupBloc = AddGroupBloc();

  @override
  Widget build(BuildContext context) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(
          double.infinity,
          115 * settingsBloc.screenSizeRelation,
        ),
        child: Navbar(
          'HabitsGroup',
          title: homeBloc.arguments != null ? 'Edit group' : 'New group',
        ),
      ),
      body: BlocProvider<AddGroupBloc>(
        blocBuilder: () => _addGroupBloc,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16 * settingsBloc.screenSizeRelation,
              ),
              child: homeBloc.arguments != null
                  ? Column(
                      children: <Widget>[
                        GroupNameInput(name: homeBloc.arguments['name']),
                        GroupColorPicker(colorValues: homeBloc.arguments['color']),
                      ],
                    )
                  : Column(
                      children: <Widget>[
                        GroupNameInput(),
                        GroupColorPicker(),
                      ],
                    ),
            ),
            Spacer(),
            homeBloc.arguments != null
                ? SendGroupButton(
                    title: 'Edit group',
                    oldName: homeBloc.arguments['name'],
                  )
                : SendGroupButton()
          ],
        ),
      ),
    );
  }
}
