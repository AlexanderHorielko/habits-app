import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/habits_bloc/habits_bloc.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/helpers/helper_functions.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/repositories/habit_repository.dart';
import 'package:intl/intl.dart';

class HabitsScreen extends StatefulWidget {
  static const routeName = '/habits';

  @override
  _HabitsScreenState createState() => _HabitsScreenState();
}

class _HabitsScreenState extends State<HabitsScreen> {
  HabitsBloc _habitsBloc = HabitsBloc();

  @override
  Widget build(BuildContext context) {
    AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return BlocProvider(
      blocBuilder: () => _habitsBloc,
      child: Container(
        height: 370 * settingsBloc.screenSizeRelation,
        margin: EdgeInsets.only(
          top: 30 * settingsBloc.screenSizeRelation,
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 15 * settingsBloc.screenSizeRelation,
        ),
        child: StreamBuilder(
          stream: HabitRepository.getHabitsStream(authBloc.user),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            final allHabits = HabitRepository.transformToHabitsList(snapshot.data);

            List<Habit> habitsOfCurrentGroup = [];

            allHabits.forEach((element) {
              if (element.habitGroup == homeBloc.arguments['name']) {
                habitsOfCurrentGroup.add(element);
              }
            });
            return ListView.builder(
              itemCount: habitsOfCurrentGroup.length,
              itemBuilder: (context, index) {
                final created = DateFormat('dd.MM.yyyy').format(habitsOfCurrentGroup[index].created);
                return Container(
                  margin: EdgeInsets.only(
                    bottom: 20 * settingsBloc.screenSizeRelation,
                  ),
                  decoration: BoxDecoration(
                    color: settingsBloc.theme == 'White theme' ? Colors.white : Theme.of(context).primaryColorLight,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Dismissible(
                    key: ValueKey(habitsOfCurrentGroup[index].id),
                    background: Container(
                      width: double.infinity,
                      color: Theme.of(context).errorColor,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 20 * settingsBloc.screenSizeRelation,
                          ),
                          child: Icon(
                            Icons.delete,
                            size: 40 * settingsBloc.screenSizeRelation,
                            color: Theme.of(context).primaryColorLight,
                          ),
                        ),
                      ),
                    ),
                    direction: DismissDirection.endToStart,
                    onDismissed: (direction) async {
                      try {
                        final oldDays = await _habitsBloc.deleteHabit(
                          habitsOfCurrentGroup[index].id,
                        );
                        Scaffold.of(context).showSnackBar(
                          HelperFunctions.buildSnackBar(
                            context,
                            'Succefully deleted',
                            color: Theme.of(context).primaryColorDark,
                            deletedHabit: habitsOfCurrentGroup[index],
                            oldDays: oldDays,
                          ),
                        );
                      } catch (e) {
                        Scaffold.of(context).showSnackBar(
                          HelperFunctions.buildSnackBar(
                            context,
                            e.message,
                            isError: true,
                          ),
                        );
                      }
                    },
                    child: ListTile(
                      title: Text(
                        habitsOfCurrentGroup[index].name,
                        style: TextStyle(
                          fontSize: 16 * settingsBloc.screenSizeRelation,
                        ),
                      ),
                      subtitle: Text(
                        '$created • ${habitsOfCurrentGroup[index].frequency}',
                        style: TextStyle(
                          fontSize: 12 * settingsBloc.screenSizeRelation,
                        ),
                      ),
                      trailing: habitsOfCurrentGroup[index].isPrimary
                          ? SvgPicture.asset(
                              'assets/images/big_star_active.svg',
                            )
                          : Container(
                              width: 0,
                              height: 0,
                            ),
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
