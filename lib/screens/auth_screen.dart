import 'package:flutter/material.dart';
import 'package:habit/bloc/auth_bloc/auth_bloc.dart';
import 'package:habit/bloc/auth_bloc/auth_event.dart';
import 'package:habit/bloc/auth_bloc/auth_state.dart';
import 'package:habit/bloc/base/bloc_event_state_builder.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/widgets/auth_screen/auth_form.dart';

import '../main.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  Future<bool> _onWillPopScope() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    AuthBloc bloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    settingsBloc.screenSizeRelation = MediaQuery.of(context).size.height / MyApp.defaultHeight;
    settingsBloc.settingsWereChanged = false;
    return WillPopScope(
      onWillPop: _onWillPopScope,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: BlocEventStateBuilder<AuthEvent, AuthState>(
          bloc: bloc,
          builder: (BuildContext ctx, AuthState state) {
            if (state.isAuthenticating) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state.hasFailed) {
              return AuthForm(
                error: state.error,
              );
            } else {
              return AuthForm();
            }
          },
        ),
      ),
    );
  }
}
