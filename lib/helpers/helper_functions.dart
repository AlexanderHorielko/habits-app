import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_bloc.dart';
import 'package:habit/bloc/add_habit_bloc/add_habit_event.dart';
import 'package:habit/bloc/base/bloc_provider.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_bloc.dart';
import 'package:habit/bloc/bottom_navbar_bloc/bottom_navbar_event.dart';
import 'package:habit/bloc/home_bloc/home_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_bloc.dart';
import 'package:habit/bloc/settings_bloc/settings_event.dart';
import 'package:habit/models/habit.dart';
import 'package:habit/models/habit_group.dart';
import 'package:habit/repositories/days_repository.dart';
import 'package:habit/repositories/habit_repository.dart';
import 'package:habit/repositories/habits_group_repository.dart';
import 'package:habit/screens/add_group_screen.dart';

class HelperFunctions {
  static void redirectToPage(BuildContext context, Widget page) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Navigator.of(context).pushAndRemoveUntil(
        PageRouteBuilder(
          transitionDuration: const Duration(milliseconds: 200),
          pageBuilder: (context, animation, secondaryAnimation) {
            return page;
          },
        ),
        ModalRoute.withName('/'),
      );
    });
  }

  static Widget buildSnackBar(
    BuildContext context,
    String message, {
    bool isError,
    Color color,
    Habit deletedHabit,
    HabitGroup deletedHabitGroup,
    QuerySnapshot oldDays,
  }) {
    return SnackBar(
      content: Text(
        message,
        style: TextStyle(
          color: color != null && color.computeLuminance() > 0.5 ? Colors.black : Colors.white,
        ),
      ),
      backgroundColor:
          isError != null ? Theme.of(context).errorColor : color != null ? color : Theme.of(context).accentColor,
      action: deletedHabit != null || deletedHabitGroup != null
          ? SnackBarAction(
              label: 'UNDO',
              textColor: color != null && color.computeLuminance() > 0.5 ? Colors.black : Colors.white,
              onPressed: deletedHabit != null
                  ? () async {
                      await HabitRepository.addHabit(deletedHabit, id: deletedHabit.id);
                      await DaysRepository.resetHabitIntoDays(oldDays);
                    }
                  : () async {
                      await HabitsGroupRepository.addGroup(deletedHabitGroup);
                      deletedHabitGroup.habits.forEach((element) async {
                        await HabitRepository.addHabit(element, id: element.id);
                      });
                      await DaysRepository.resetHabitIntoDays(oldDays);
                    },
            )
          : null,
    );
  }

  static Widget buildOption(BuildContext context, String name, bool choosed, String type) {
    // AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    AddHabitBloc addHabitBloc = BlocProvider.of<AddHabitBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return Container(
      width: 100 * settingsBloc.screenSizeRelation,
      height: 40 * settingsBloc.screenSizeRelation,
      decoration: BoxDecoration(
        color: choosed
            ? Theme.of(context).accentColor
            : settingsBloc.theme == 'White theme'
                ? Theme.of(context).primaryColorLight
                : Color.fromRGBO(46, 46, 46, 46),
        border: choosed
            ? Border.all(color: Colors.transparent)
            : Border.all(
                color: Theme.of(context).primaryColorDark,
              ),
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Center(
            child: Text(
              name,
              style: TextStyle(
                fontSize: 14 * settingsBloc.screenSizeRelation,
                fontWeight: FontWeight.w400,
                color: choosed ? Theme.of(context).primaryColorLight : Theme.of(context).primaryColorDark,
              ),
            ),
          ),
          onTap: () {
            if (type == 'languageOption') {
              settingsBloc.language = name;
              settingsBloc.emitEvent(SettingsEventSwitchLanguage(name));
            } else if (type == 'habitsOption') {
              addHabitBloc.emitEvent(AddHabitEventChangeGroup(name));
            } else {
              homeBloc.arguments = null;
              Navigator.of(context).pushNamed(AddGroupScreen.routeName);
            }
          },
        ),
      ),
    );
  }

  static Widget buildAddButton(BuildContext context, String title, String routeName, {dynamic arguments}) {
    SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    BottomNavbarBloc bottomNavbarBloc = BlocProvider.of<BottomNavbarBloc>(context);
    HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);

    return Container(
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(
              16 * settingsBloc.screenSizeRelation,
            ),
            topRight: Radius.circular(
              16 * settingsBloc.screenSizeRelation,
            ),
          ),
        ),
        padding: EdgeInsets.all(
          20 * settingsBloc.screenSizeRelation,
        ),
        color: Theme.of(context).accentColor,
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
              fontSize: 16 * settingsBloc.screenSizeRelation,
            ),
          ),
        ),
        onPressed: () {
          if (routeName == AddGroupScreen.routeName) {
            homeBloc.arguments = null;
            Navigator.of(context).pushNamed(routeName);
          } else {
            Navigator.of(context).pushNamed(routeName, arguments: arguments);
          }
        },
      ),
    );
  }

  static Color buildColor(List colorValues) {
    return Color.fromRGBO(
      colorValues[0],
      colorValues[1],
      colorValues[2],
      double.parse(
        colorValues[3].toString(),
      ),
    );
  }

  static String getHabitsEnding(int length) {
    return length == 1 ? 'habit' : 'habits';
  }

  static DateTime getDateTimeFromTimestamp(Timestamp date) {
    return DateTime.fromMillisecondsSinceEpoch(date.seconds * 1000);
  }

  static int getHabitFrequency(Habit habit) {
    int frequency = 0;
    switch (habit.frequency) {
      case 'Every day':
        frequency = 0;
        break;
      case 'In a day':
        frequency = 2;
        break;
      case 'In 2 days':
        frequency = 3;
        break;
      case 'Once a week':
        frequency = 7;
        break;
      default:
    }
    return frequency;
  }

  static List<List<DateTime>> getHabitStreak(Habit habit, QuerySnapshot days) {
    List<DateTime> allPresenceDates = [];
    List<List<DateTime>> streaks = [];

    days.documents.forEach(
      (element) {
        final DateTime dateFromTimeStamp = HelperFunctions.getDateTimeFromTimestamp(element['date']);
        final dateKey = DateTime(
          dateFromTimeStamp.year,
          dateFromTimeStamp.month,
          dateFromTimeStamp.day,
        );

        if (element['habits'].containsKey(habit.id)) {
          if (element['habits'][habit.id]) {
            allPresenceDates.add(dateKey);
          }
        }
      },
    );

    allPresenceDates.sort((a, b) => a.compareTo(b));
    int frequency = 0;

    frequency = HelperFunctions.getHabitFrequency(habit);

    int groupCounter = 0;
    bool haveLastStreak = false;
    streaks = [];
    for (var i = 0; i < allPresenceDates.length - 1; i++) {
      final difference = allPresenceDates[i].difference(allPresenceDates[i + 1]).inDays;

      if (frequency == 0) frequency = 1;
      if (difference.abs() == frequency) {
        haveLastStreak = true;
        if (streaks.length <= groupCounter) {
          streaks.add([]);
          streaks[groupCounter].add(allPresenceDates[i]);
          streaks[groupCounter].add(allPresenceDates[i + 1]);
        } else {
          streaks[groupCounter].add(allPresenceDates[i + 1]);
        }
      } else {
        if (haveLastStreak) {
          groupCounter++;
        }
        haveLastStreak = false;
      }
    }

    return streaks;
  }

  static bool getDayStreaks(DocumentSnapshot day) {
    bool isAllDone = true;

    if (day.data['habits'].length != 0) {
      day.data['habits'].forEach(
        (key, element) {
          if (!element) {
            isAllDone = false;
          }
        },
      );
    } else {
      isAllDone = false;
    }

    return isAllDone;
  }
}
