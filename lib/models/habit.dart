import 'package:flutter/foundation.dart';

class Habit {
  String id;
  String name;
  String habitGroup;
  String frequency;
  bool isPrimary;
  bool remindHabits;
  bool isDone;
  DateTime remindTime;
  DateTime created;

  Habit({
    this.id,
    @required this.name,
    @required this.habitGroup,
    @required this.frequency,
    @required this.isPrimary,
    @required this.remindHabits,
    @required this.isDone,
    @required this.remindTime,
    @required this.created,
  });
}
