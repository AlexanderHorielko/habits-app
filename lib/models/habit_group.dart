class HabitGroup {
  String id;
  String name;
  List<dynamic> colorValues;
  List<dynamic> habits;

  HabitGroup(this.name, this.colorValues, {this.id, this.habits});
}
